﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PokeApi;
using Utility;

public class Controller : MonoBehaviour {

    private DataFetcher fetcher = new DataFetcher();
    private readonly Cache cache = new Cache(false);
    private CoroutineData _cr;

    // Use this for initialization
    IEnumerator Start() {
        LimitedType obj = new LimitedType(new []{typeof(int), typeof(float)});
        obj.Assign(3.5f);
        print(obj.Get<float>());
        yield return new WaitForSeconds(0.5f);

        //print("Get Pokemon at " + Time.time);
        //var crPokemon = new CoroutineData<Pokemon>(this, cache.GetObject<Pokemon>("/pokemon/1"));
        //while (true) {
        //    if (crPokemon.IsDone)
        //        break;
        //    yield return null;
        //}
        //print("Done Pokemon at " + Time.time);
        //var pokemon = /*(Pokemon)*/crPokemon.Result;
        //print("Species: " + pokemon.Species.Name);

        //print("Get pages at " + Time.time);
        //var crList = new CoroutineData<List<NamedAPIResource<Pokemon>>>(this, cache.GetAllNames<Pokemon>());
        //yield return crList.Coroutine;
        //print("Done pages at " + Time.time);
        //foreach (var pokemonJson in crList.Result) {
        //    yield return cache.GetJson(pokemonJson.Url);
        //    var pokemon = JsonUtility.FromJson<Pokemon>(cache.LastJson);
        //    print("Name: " + pokemon.Name);
        //}
        //print("Done!");
        //cache.Save();
    }

    // Update is called once per frame
    void Update() { }

}
