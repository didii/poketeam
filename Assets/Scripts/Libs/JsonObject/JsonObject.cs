#define OPERATORS

using System;
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;


public class JsonObject {
    #region Underlying Types
    /// <summary>
    /// Possible types of the object
    /// </summary>
    public enum EType { Null, Bool, Number, String, Array, Object }

    /// <summary>
    /// Helper struct which holds the values
    /// </summary>
    private struct JsonValue {
        #region Fields
        private EType _type;
        private bool? _bool;
        private float? _number;
        private string _string;
        private List<JsonObject> _array;
        private Dictionary<string, JsonObject> _object;
        #endregion

        #region Properties
        /// <summary>
        /// The value type that is not null, if <see cref="Type"/>==<see cref="EType.Null"/> all
        /// values are null. When setting, it invalidates all other values and set the default
        /// value of the given type.
        /// </summary>
        public EType Type {
            get { return _type; }
            set {
                if (_type == value)
                    return;
                ClearValues();
                switch (value) {
                case EType.Null:
                    break;
                case EType.Bool:
                    _bool = default(bool);
                    break;
                case EType.Number:
                    _number = default(float);
                    break;
                case EType.String:
                    _string = default(string);
                    break;
                case EType.Array:
                    _array = new List<JsonObject>();
                    break;
                case EType.Object:
                    _object = new Dictionary<string, JsonObject>();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("value", value, null);
                }
                _type = value;
            }
        }
        /// <summary>
        /// The bool value. Throws <see cref="InvalidOperationException"/> if type mismatch.
        /// </summary>
        public bool Bool {
            get { return _bool.Value; }
            set {
                HotSwapType(EType.Bool);
                _bool = value;
            }
        }
        /// <summary>
        /// The numeric value. Throws <see cref="InvalidOperationException"/> if type mismatch.
        /// </summary>
        public float Number {
            get { return _number.Value; }
            set {
                HotSwapType(EType.Number);
                _number = value;
            }
        }
        /// <summary>
        /// The string value. Throws <see cref="InvalidOperationException"/> if type mismatch.
        /// </summary>
        public string String {
            get { return _string; }
            set {
                HotSwapType(EType.String);
                _string = value;
            }
        }
        /// <summary>
        /// The array value. Throws <see cref="InvalidOperationException"/> if type mismatch.
        /// </summary>
        public List<JsonObject> Array {
            get { return _array; }
            set {
                HotSwapType(EType.Array);
                _array = value;
            }
        }
        /// <summary>
        /// The object value. Throws <see cref="InvalidOperationException"/> if type mismatch.
        /// </summary>
        public Dictionary<string, JsonObject> Object {
            get { return _object; }
            set {
                HotSwapType(EType.Object);
                _object = value;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Checks equality with object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is JsonValue && Equals((JsonValue)obj);
        }

        /// <summary>
        /// Check whether the type and the non-null type are equal. Uses Object.Equals when possible.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(JsonValue other) {
            if (_type != other._type)
                return false;
            switch (_type) {
            case EType.Null:
                return true;
            case EType.Bool:
                return _bool == other._bool;
            case EType.Number:
                return _number.Equals(other._number);
            case EType.String:
                return string.Equals(_string, other._string);
            case EType.Array:
                return Equals(_array, other._array);
            case EType.Object:
                return Equals(_object, other._object);
            default:
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Generates a Hash Code based on the existing type.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() {
            unchecked {
                var hashCode = (int)_type;
                switch (_type) {
                case EType.Null:
                    break;
                case EType.Bool:
                    hashCode = (hashCode * 397) ^ _bool.GetHashCode();
                    break;
                case EType.Number:
                    hashCode = (hashCode * 397) ^ _number.GetHashCode();
                    break;
                case EType.String:
                    hashCode = (hashCode * 397) ^ (_string != null ? _string.GetHashCode() : 0);
                    break;
                case EType.Array:
                    hashCode = (hashCode * 397) ^ (_array != null ? _array.GetHashCode() : 0);
                    break;
                case EType.Object:
                    hashCode = (hashCode * 397) ^ (_object != null ? _object.GetHashCode() : 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
                }
                return hashCode;
            }
        }

        #endregion

        #region Helpers
        /// <summary>
        /// Switch type without setting the default value immediately
        /// </summary>
        /// <param name="type"></param>
        private void HotSwapType(EType type) {
            if (_type == type)
                return;
            ClearValues();
            _type = type;
        }
        /// <summary>
        /// Sets all values to null
        /// </summary>
        private void ClearValues() {
            _bool = null;
            _number = null;
            _string = null;
            _array = null;
            _object = null;
        }

        #endregion
    }

    /// <summary>
    /// Some rules Json likes
    /// </summary>
    private struct JsonRules {
        /// <summary>
        /// Indicates start of object
        /// </summary>
        public const char ObjectStartChar = '{';
        /// <summary>
        /// Indicates end of object
        /// </summary>
        public const char ObjectEndChar = '}';
        /// <summary>
        /// Indicates seperator between key and value of an object
        /// </summary>
        public const char ObjectPairSeparatorChar = ':';
        /// <summary>
        /// Indicates start of an array
        /// </summary>
        public const char ArrayStartChar = '[';
        /// <summary>
        /// Indicates end of an array
        /// </summary>
        public const char ArrayEndChar = ']';
        /// <summary>
        /// Indicates seperator between array and object values
        /// </summary>
        public const char ValueSeparatorChar = ',';
        /// <summary>
        /// 
        /// </summary>
        public const char StringChar = '"';
        public const char EscapeChar = '\\';

        public static char ToEscape(char escape) {
            switch (escape) {
            case '"':
            case '\\':
            case '/':
                return escape;
            case 'b':
                return '\b';
            case 'f':
                return '\f';
            case 'n':
                return '\n';
            case 'r':
                return '\r';
            case 't':
                return '\t';
            default:
                throw new Exception("Not an escape character");
            }
        }

        public static bool TryToEscape(char escape, ref char escaped) {
            try {
                escaped = ToEscape(escape);
                return true;
            }
            catch (Exception) {
                return false;
            }
        }
    }

    #endregion

    #region Fields
    /// <summary>
    /// Maximum recursion depth
    /// </summary>
    public const int MaxDepth = 100;
    /// <summary>
    /// All characters that can be skipped over
    /// </summary>
    private static readonly char[] WhitespaceChars = { ' ', '\r', '\n', '\t', '\uFEFF', '\u0009' };

    /// <summary>
    /// The value this object holds
    /// </summary>
    private JsonValue _jsonValue;
    /// <summary>
    /// The parent of this object
    /// </summary>
    private JsonObject _parent;

    #endregion

    #region Properties
    /// <summary>
    /// The type this object holds. Always check before asking a value.
    /// </summary>
    public EType Type {
        get { return _jsonValue.Type; }
        set { _jsonValue.Type = value; }
    }
    /// <summary>
    /// Does the object hold any value?
    /// </summary>
    public bool IsNull {
        get { return Type == EType.Null; }
    }

    /// <summary>
    /// Boolean value of this object.
    /// </summary>
    /// <exception cref="InvalidOperationException">If <see cref="Type"/> is not <see cref="EType.Bool"/></exception>
    public bool BoolValue {
        get { return _jsonValue.Bool; }
        private set { _jsonValue.Bool = value; }
    }
    /// <summary>
    /// Integer value of this object.
    /// </summary>
    /// <exception cref="InvalidOperationException">If <see cref="Type"/> is not <see cref="EType.Number"/></exception>
    public int IntValue {
        get { return Mathf.RoundToInt(FloatValue); }
        private set { FloatValue = value; }
    }
    /// <summary>
    /// Float value of this object.
    /// </summary>
    /// <exception cref="InvalidOperationException">If <see cref="Type"/> is not <see cref="EType.Number"/></exception>
    public float FloatValue {
        get { return _jsonValue.Number; }
        private set { _jsonValue.Number = value; }
    }
    /// <summary>
    /// String value of this object. Is null if not the type.
    /// </summary>
    public string StringValue {
        get { return _jsonValue.String; }
        private set { _jsonValue.String = value; }
    }
    /// <summary>
    /// Array (=<see cref="List{JsonObject}"/>) value of this object. Is null if not the type.
    /// </summary>
    public List<JsonObject> ArrayValue {
        get { return _jsonValue.Array; }
        private set { _jsonValue.Array = value; }
    }
    /// <summary>
    /// Object (=<see cref="Dictionary{String,JsonObject}"/> value of this object. Is null if not the type.
    /// </summary>
    public Dictionary<string, JsonObject> ObjectValue {
        get { return _jsonValue.Object; }
        private set { _jsonValue.Object = value; }
    }

    /// <summary>
    /// Number of objects within this object. Returns null if not an <see cref="EType.Array"/> or <see cref="EType.Object"/>.
    /// </summary>
    public int? Count {
        get {
            switch (_jsonValue.Type) {
            case EType.Array:
                return ArrayValue.Count;
            case EType.Object:
                return ObjectValue.Count;
            default:
                return null;
            }
        }
    }

    /// <summary>
    /// The parent of this object which is always a container.
    /// </summary>
    public JsonObject Parent {
        get { return _parent; }
    }

    #endregion

    #region Constructor

    public JsonObject() { }

    public JsonObject(bool @bool) {
        Type = EType.Bool;
        BoolValue = @bool;
    }

    public JsonObject(int @int) {
        Type = EType.Number;
        IntValue = @int;
    }

    public JsonObject(float @float) {
        Type = EType.Number;
        FloatValue = @float;
    }

    public JsonObject(string @string) {
        Type = EType.String;
        StringValue = @string;
    }

    public JsonObject(string key, JsonObject val) : this(EType.Object) {
        Add(key, val);
    }

    public JsonObject(IEnumerable<JsonObject> array) {
        Type = EType.Array;
        ArrayValue = new List<JsonObject>(array);
    }

    public JsonObject(Dictionary<string, JsonObject> obj) {
        Type = EType.Object;
        ObjectValue = new Dictionary<string, JsonObject>(obj);
    }

    public JsonObject(EType type) {
        Type = type;
        switch (type) {
        case EType.Array:
            ArrayValue = new List<JsonObject>();
            break;
        case EType.Object:
            ObjectValue = new Dictionary<string, JsonObject>();
            break;
        }
    }

    public JsonObject(JsonObject obj) {
        _jsonValue = obj._jsonValue;
    }

    #endregion

    #region Factories

    public static JsonObject Null {
        get { return new JsonObject(EType.Null); }
    }

    public static JsonObject Array {
        get { return new JsonObject(EType.Array); }
    }

    public static JsonObject Object {
        get { return new JsonObject(EType.Object); }
    }

    #endregion

    #region Methods

    public void Add(JsonObject obj) {
        if (Type == EType.Null)
            Type = EType.Array; // only promote if null
        if (Type != EType.Array)
            throw new InvalidJsonObjectTypeException(EType.Array);
        ArrayValue.Add(obj.SetParent(this));
    }

    public void Add(string name, JsonObject obj) {
        if (Type != EType.Object)
            throw new InvalidJsonObjectTypeException(EType.Object);
        ObjectValue.Add(name, obj.SetParent(this));
    }

    public void Remove(JsonObject obj) {
        if (Type != EType.Array)
            throw new InvalidJsonObjectTypeException(EType.Array);
        ArrayValue.Remove(obj);
    }

    public void RemoveObject(string key) {
        if (Type != EType.Object)
            throw new InvalidJsonObjectTypeException(EType.Object);
        ObjectValue.Remove(key);
    }

    public void Clear() {
        Type = EType.Null;
    }

    #endregion

    #region Parsing
    /// <summary>
    /// Converts this object to its Json string representation.
    /// </summary>
    /// <returns>The json string</returns>
    public string ToJsonString() {
        return ToJson(this);
    }

    /// <summary>
    /// Convert any JsonObject to its Json string representation.
    /// </summary>
    /// <param name="obj">The object to convert</param>
    /// <returns>The json string</returns>
    public static string ToJson(JsonObject obj) {
        return ToJsonRecur(obj, 0);
    }

    #region ToJson Helpers
    /// <summary>
    /// Start of recursive ToJson
    /// </summary>
    /// <param name="obj">The json object to convert</param>
    /// <param name="depth">Current depth</param>
    /// <returns>The json string</returns>
    private static string ToJsonRecur(JsonObject obj, int depth) {
        if (depth > MaxDepth)
            throw new MaxJsonObjectRecursionDepthException(MaxDepth, obj);
        switch (obj.Type) {
        case EType.Null:
        case EType.Bool:
        case EType.Number:
        case EType.String:
            return obj.ToString();
        case EType.Array:
            return ArrayToJson(obj.ArrayValue, depth + 1);
        case EType.Object:
            return ObjectToJson(obj.ObjectValue, depth + 1);
        default:
            throw new ArgumentOutOfRangeException();
        }
    }
    /// <summary>
    /// Converts the given <see cref="JsonObject"/> list to its json string representation.
    /// </summary>
    /// <param name="list">The list to convert</param>
    /// <param name="depth">Current depth</param>
    /// <returns>The json string</returns>
    private static string ArrayToJson(List<JsonObject> list, int depth) {
        var result = "[";
        if (list.Count > 0)
            result += list.Select(i => ToJsonRecur(i, depth + 1)).Aggregate((a, b) => a + "," + b);
        result += "]";
        return result;
    }
    /// <summary>
    /// Converts the given <see cref="Dictionary{String,JsonObject}"/> to its json string representation.
    /// </summary>
    /// <param name="dic">The dictionary to convert</param>
    /// <param name="depth">The current depth</param>
    /// <returns>The json string</returns>
    private static string ObjectToJson(Dictionary<string, JsonObject> dic, int depth) {
        var result = "{";
        if (dic.Count > 0)
            result += dic.Select(pair => "\"" + pair.Key + "\"" + ":" + ToJsonRecur(pair.Value, depth + 1)).Aggregate((a, b) => a + "," + b);
        result += "}";
        return result;
    }

    #endregion

    /// <summary>
    /// Converts the given string to a <see cref="JsonObject"/>.
    /// </summary>
    /// <param name="json">The json string</param>
    /// <returns>The corresponding <see cref="JsonObject"/></returns>
    public static JsonObject FromJson(string json) {
        return FromJson(new StringReader(json));
    }
    /// <summary>
    /// Converts the given stream (e.g. <see cref="FileStream"/>) to a <see cref="JsonObject"/>.
    /// </summary>
    /// <param name="stream">The stream containing the json string</param>
    /// <returns>The corresponding <see cref="JsonObject"/></returns>
    public static JsonObject FromJson(TextReader stream) {
        var helper = new FromJsonHelper(stream);
        return helper.Parse();
    }

    #region FromJson helpers
    /// <summary>
    /// Sets the parent of this to <paramref name="parent"/> while returning this.
    /// </summary>
    /// <param name="parent"></param>
    /// <returns></returns>
    private JsonObject SetParent(JsonObject parent) {
        this._parent = parent;
        return this;
    }

    /// <summary>
    /// A helper struct so the <see cref="TextReader"/> doesn't has to be in every single function
    /// parameter and does not clutter the <see cref="JsonObject"/> class. It parses the stream.
    /// </summary>
    private struct FromJsonHelper {
        #region Fields
        /// <summary>
        /// The reader to read from.
        /// </summary>
        private readonly TextReader _reader;
        #endregion

        #region Constructor
        /// <summary>
        /// Construct using a reader
        /// </summary>
        /// <param name="reader"></param>
        public FromJsonHelper(TextReader reader) {
            _reader = reader;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Parse the reader
        /// </summary>
        /// <returns>The parsed <see cref="JsonObject"/></returns>
        public JsonObject Parse() {
            return ParseRecur(ReadSingle(), 0);
        }
        #endregion

        #region Helper methods
        /// <summary>
        /// Start of recursive parsing.
        /// </summary>
        /// <param name="c">The first non-whitespace character</param>
        /// <param name="depth">The current depth</param>
        /// <returns>The parsed <see cref="JsonObject"/></returns>
        private JsonObject ParseRecur(char c, int depth) {
            if (depth > MaxDepth)
                throw new MaxJsonObjectRecursionDepthException(MaxDepth);

            JsonObject result;
            if (TryParseString(c, out result))
                return result;
            if (TryParseArray(c, out result, depth))
                return result;
            if (TryParseObject(c, out result, depth))
                return result;
            if (TryParseValue(c, out result)) // Needs to be last!
                return result;
            throw new InvalidJsonFormatException("Could not identify value");
        }

        /// <summary>
        /// Try to parse a value (bool or float)
        /// </summary>
        /// <param name="c">The first non-whitespace character from the value</param>
        /// <param name="result">The parsed value</param>
        /// <returns>Whether the parse was succesfull</returns>
        private bool TryParseValue(char c, out JsonObject result) {
            result = Null;
            var s = c + PeekUntil(i => i == JsonRules.ValueSeparatorChar ||
                                       i == JsonRules.ObjectEndChar ||
                                       i == JsonRules.ArrayEndChar);
            bool @bool;
            float @float;
            if (bool.TryParse(s, out @bool)) {
                result = @bool;
                return true;
            }
            if (float.TryParse(s, out @float)) {
                result = @float;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Try to parse a string ("...")
        /// </summary>
        /// <param name="c">The first non-whitespace character of this value</param>
        /// <param name="result">The parsed value</param>
        /// <returns>Whether the parse was succesfull</returns>
        private bool TryParseString(char c, out JsonObject result) {
            result = Null;
            if (c != JsonRules.StringChar)
                return false;
            var str = "";
            while (true) {
                c = ReadSingle(false);
                if (c == JsonRules.EscapeChar) {
                    str += JsonRules.ToEscape(ReadSingle(false));
                    continue;
                }
                if (c == JsonRules.StringChar)
                    break;
                str += c;
            }
            result = str;
            return true;
        }
        /// <summary>
        /// Try to parse an array ([...,...])
        /// </summary>
        /// <param name="c">The first non-whitespace character of this value</param>
        /// <param name="result">The parsed value</param>
        /// <param name="depth">The current depth</param>
        /// <returns>Whether the parse was succesfull</returns>
        /// <exception cref="InvalidJsonFormatException"></exception>
        private bool TryParseArray(char c, out JsonObject result, int depth) {
            result = Null;
            if (c != JsonRules.ArrayStartChar)
                return false;
            result.Type = EType.Array;
            c = ReadSingle();
            if (c == JsonRules.ArrayEndChar)
                return true;
            while (true) {
                result.Add(ParseRecur(c, depth + 1).SetParent(result));
                c = ReadSingle();
                if (c == JsonRules.ValueSeparatorChar) {
                    c = ReadSingle();
                    continue;
                }
                if (c != JsonRules.ArrayEndChar)
                    throw new InvalidJsonFormatException("Expected either ',' or ']' after array value");
                break;
            }
            return true;
        }
        /// <summary>
        /// Try to parse an object ({"...":...,"...":...})
        /// </summary>
        /// <param name="c">The first non-whitespace character of this value</param>
        /// <param name="result">The parsed value</param>
        /// <param name="depth">The current depth</param>
        /// <returns>Whether the parse was succesfull</returns>
        /// <exception cref="InvalidJsonFormatException"></exception>
        private bool TryParseObject(char c, out JsonObject result, int depth) {
            result = Null;
            if (c != JsonRules.ObjectStartChar)
                return false;
            result.Type = EType.Object;
            c = ReadSingle();
            if (c == JsonRules.ObjectEndChar)
                return true;
            while (true) {
                JsonObject key;
                if (!TryParseString(c, out key))
                    throw new InvalidJsonFormatException("Expected string as key for object");
                c = ReadSingle();
                if (c != JsonRules.ObjectPairSeparatorChar)
                    throw new InvalidJsonFormatException("Expected ':' after key of object");
                c = ReadSingle();
                result.Add(key.StringValue, ParseRecur(c, depth + 1));
                c = ReadSingle();
                if (c == JsonRules.ValueSeparatorChar) {
                    c = ReadSingle();
                    continue;
                }
                if (c != JsonRules.ObjectEndChar)
                    throw new InvalidJsonFormatException("Expected ',' or '}' after object value");
                break;
            }
            return true;
        }

        /// <summary>
        /// Read a single character with an option to skip whitspace characters.
        /// </summary>
        /// <param name="skipWhitespace">Keep reading until non-whitespace character?</param>
        /// <returns>The first (non-whitespace) character</returns>
        private char ReadSingle(bool skipWhitespace = true) {
            while (true) {
                int i = _reader.Read();
                if (i < 0) throw new Exception("Unexpected EOF");
                var c = (char)i;
                if (skipWhitespace && WhitespaceChars.Contains(c)) continue;
                return (char)i;
            }
        }
        /// <summary>
        /// Returns a string from the current position until the <paramref name="condition"/> or
        /// EOF is true. The stream position is now at the condition. Thus 
        /// <c>condition(_reader.Read()) == true</c>.
        /// </summary>
        /// <param name="condition">Stop reading at when this is true</param>
        /// <returns></returns>
        private string PeekUntil(Func<char, bool> condition) {
            var result = "";
            while (true) {
                var i = _reader.Peek();
                if (i < 0) return result;
                var c = (char)i;
                if (condition(c))
                    return result;
                result += (char)_reader.Read();
            }
        }
        #endregion
    }

    #endregion

    #endregion

    #region Object overrides
    /// <summary>
    /// Default equals method referring to <see cref="Equals(JsonObject)"/>.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj) {
        if (ReferenceEquals(null, obj))
            return false;
        if (ReferenceEquals(this, obj))
            return true;
        if (!(obj is JsonObject))
            return false;
        return Equals((JsonObject)obj);
    }
    /// <summary>
    /// Equals the underlying values. When of type <see cref="EType.Array"/> or <see cref="EType.Object"/>, it checks by refernce.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    protected bool Equals(JsonObject other) {
        return _jsonValue.Equals(other._jsonValue);
    }

    /// <summary>
    /// Same Hash Code as its underlying value.
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode() {
        return _jsonValue.GetHashCode();
    }

    public override string ToString() {
        switch (Type) {
        case EType.Null:
            return "null";
        case EType.Bool:
            return BoolValue.ToString().ToLower();
        case EType.Number:
            return FloatValue.ToString(CultureInfo.InvariantCulture);
        case EType.String:
            return "\"" + StringValue + "\"";
        case EType.Array:
            return "Array[Count=" + ArrayValue.Count + "]";
        case EType.Object:
            return "Object[Count=" + ObjectValue.Count + "]";
        default:
            throw new ArgumentOutOfRangeException();
        }
    }

    #endregion

    #region Operators

#if OPERATORS
    /// <summary>
    /// Allows for <c>JsonObject jObj = true;</c>.
    /// </summary>
    /// <param name="bool"></param>
    public static implicit operator JsonObject(bool @bool) {
        return new JsonObject(@bool);
    }
    /// <summary>
    /// Allows for <c>JsonObject jObj = 3;</c>.
    /// </summary>
    /// <param name="int"></param>
    public static implicit operator JsonObject(int @int) {
        return new JsonObject(@int);
    }
    /// <summary>
    /// Allows for <c>JsonObject jObj = 3.125f;</c>.
    /// </summary>
    /// <param name="float"></param>
    public static implicit operator JsonObject(float @float) {
        return new JsonObject(@float);
    }
    /// <summary>
    /// Allows for <c>JsonObject jObj = "foo";</c>.
    /// </summary>
    /// <param name="string"></param>
    public static implicit operator JsonObject(string @string) {
        return new JsonObject(@string);
    }
#endif

    #endregion
}

/// <summary>
/// Thrown when json string format is invalid
/// </summary>
public class InvalidJsonFormatException : Exception {
    public InvalidJsonFormatException() : base("Invalid Json format") { }
    public InvalidJsonFormatException(string msg) : base(msg) { }
}

/// <summary>
/// Thrown when the EOF was reached while parsing was not yet finished.
/// </summary>
public class UnexpectedEndOfFileException : Exception {
    public UnexpectedEndOfFileException()
        : base("Unexpected end of file") { }
}

/// <summary>
/// Thrown when trying to change the <see cref="JsonObject"/> in a way the underlying type is not made for.
/// </summary>
public class InvalidJsonObjectTypeException : Exception {
    public InvalidJsonObjectTypeException(JsonObject.EType type) : base("JsonObject type mismatch: needs to be of type " + type) { }

    public InvalidJsonObjectTypeException(JsonObject.EType[] types)
        : base(
            "JsonObject type mismatch: needs to be of types " +
            types.Select(a => a.ToString()).Aggregate((a, b) => a + " or " + b)) { }
}

/// <summary>
/// Thrown when parsing depth exceeded <see cref="JsonObject.MaxDepth"/>.
/// </summary>
public class MaxJsonObjectRecursionDepthException : Exception {
    private const string Msg = "Max recursion depth %i reached. Set MaxDepth to a higher value to allow this.";
    public JsonObject InputValue;

    public MaxJsonObjectRecursionDepthException(int maxDepth) :
        base(string.Format(Msg, maxDepth)) {
    }

    public MaxJsonObjectRecursionDepthException(int maxDepth, JsonObject inputValue) :
        base(string.Format(Msg, maxDepth)) {
        InputValue = inputValue;
    }

}