﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using UnityEngine.Assertions;
using Utility;

[CustomEditor(typeof(UnitTester))]
public partial class UnitTesterEditor : Editor {

    private UnitTester _self;

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        _self = (UnitTester)target;
        if (GUILayout.Button("Run Unit Tests")) {
            Test();
        }
    }

    /// <summary>
    /// Finds and executes all methods declared below
    /// </summary>
    public void Test() {
        Assert.raiseExceptions = true;
        if (_self.TestsToRun == UnitTester.Tests.QuickTest) {
            QuickTest();
            Debug.Log("QuickTest finished");
            return;
        }

        // Find all nested types
        var nestedTypes =
            typeof(UnitTesterEditor).GetNestedTypes(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                                   BindingFlags.DeclaredOnly);
        // Go through each of them
        foreach (var type in nestedTypes) {
            // If selected
            if (_self.TestsToRun == UnitTester.Tests.All || type.Name == _self.TestsToRun.ToString()) {
                // Find all methods
                var methods =
                    type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly |
                                    BindingFlags.Static);
                Debug.Log("Testing " + methods.Length + " methods of module " + type.Name + ".\n- " +
                      methods.Select(method => method.Name).Aggregate((a, b) => a + "\n- " + b));
                var failed = new List<string>(methods.Length);
                // Go through all methods
                foreach (var method in methods) {
                    try {
                        // Run them
                        method.Invoke(null, null);
                    }
                    catch (Exception) {
                        if (_self.StopOnError)
                            throw;
                        failed.Add(method.Name);
                    }
                }
                // Show result
                if (failed.Count > 0)
                    Debug.LogError("Module " + type.Name + ": failed " + failed.Count + "/" + methods.Length + " tests.\n- " +
                        failed.Aggregate((a, b) => a + "\n- " + b));
                else
                    Debug.Log("Module " + type.Name + ": succes!");
            }
        }
    }

}
