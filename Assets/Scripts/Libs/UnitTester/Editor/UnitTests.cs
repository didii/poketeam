﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Utility;

// ReSharper disable UnusedMember.Local
/// <summary>
/// Define your tests here
/// </summary>
[UsedImplicitly]
public partial class UnitTesterEditor {

    /// <summary>
    /// Leave this test here. It is executed when QuickTest is selected.
    /// </summary>
    private void QuickTest() {

    }

    ////////////////////////////////////////////////////////////////////////////////
    //                 Define your tests below within a struct                    //
    ////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Tests the <see cref="JsonObject"/> class.
    /// </summary>
    private struct JsonObjectTest {
        private static JsonObject jObj = new JsonObject();

        private static void Empty() {
            jObj.Clear();
            Assert.IsTrue(jObj.IsNull);
        }

        private static void Bool() {
            jObj = new JsonObject(true);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Bool);
            Assert.IsTrue(jObj.BoolValue);
        }

        private static void InitInt() {
            jObj = new JsonObject(3);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(3, jObj.IntValue);
        }

        private static void InitFloat() {
            jObj = new JsonObject(3.2f);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(3.2f, jObj.FloatValue);
        }

        private static void InitString() {
            jObj = new JsonObject("foo");
            Assert.IsTrue(jObj.Type == JsonObject.EType.String);
            Assert.AreEqual("foo", jObj.StringValue);
        }

        private static void Array() {
            var a = new List<JsonObject> { 1, 2.3f, "foo", false };
            jObj = new JsonObject(a);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Array);
            for (int i = 0; i < a.Count; i++) {
                var obj = jObj.ArrayValue[i];
                switch (i) {
                case 0:
                    Assert.IsTrue(obj.Type == JsonObject.EType.Number);
                    Assert.AreEqual(a[i], obj.IntValue);
                    break;
                case 1:
                    Assert.IsTrue(obj.Type == JsonObject.EType.Number);
                    Assert.AreEqual(a[i], obj.FloatValue);
                    break;
                case 2:
                    Assert.IsTrue(obj.Type == JsonObject.EType.String);
                    Assert.AreEqual(a[i], obj.StringValue);
                    break;
                case 3:
                    Assert.IsTrue(obj.Type == JsonObject.EType.Bool);
                    Assert.AreEqual(a[i], obj.BoolValue);
                    break;
                default:
                    throw new IndexOutOfRangeException();
                }
            }
        }

        private static void Object() {
            var a = new Dictionary<string, JsonObject> {
                {"1", true}, {"2", 2}, {"3", "foo"}
            };
            jObj = new JsonObject(a);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Object);
            for (int i = 0; i < a.Count; i++) {
                var obj = jObj.ObjectValue[(i + 1).ToString()];
                switch (i) {
                case 0:
                    Assert.IsTrue(obj.Type == JsonObject.EType.Bool);
                    Assert.AreEqual(true, obj.BoolValue);
                    break;
                case 1:
                    Assert.IsTrue(obj.Type == JsonObject.EType.Number);
                    Assert.AreEqual(2, obj.IntValue);
                    break;
                case 2:
                    Assert.IsTrue(obj.Type == JsonObject.EType.String);
                    Assert.AreEqual("foo", obj.StringValue);
                    break;
                default:
                    throw new IndexOutOfRangeException();
                }
            }
        }

        private static void Add() {
            jObj.Clear();
            jObj.Add(true);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Array);
            Assert.AreEqual(true, jObj.ArrayValue[0].BoolValue);
            jObj.Add(3);
            Assert.AreEqual(3, jObj.ArrayValue[1].IntValue);
        }

        private static void AddWrong() {
            jObj.Clear();
            jObj.Type = JsonObject.EType.Number;
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            try {
                jObj.Add(3.5f);
                throw new Exception();
            }
            catch (InvalidJsonObjectTypeException) { }
        }

        private static void ToJson() {
            jObj = new JsonObject(true);
            File.WriteAllText("bool.json", jObj.ToJsonString());
            jObj = new JsonObject(3);
            File.WriteAllText("int.json", jObj.ToJsonString());
            jObj = new JsonObject(3.5f);
            File.WriteAllText("float.json", jObj.ToJsonString());
            jObj = new JsonObject("foo");
            File.WriteAllText("string.json", jObj.ToJsonString());
            jObj = new JsonObject(new JsonObject[] { 1, 3.2f, "foo", new JsonObject(new JsonObject[] { 1, 2f }), false });
            File.WriteAllText("array.json", jObj.ToJsonString());
            jObj = new JsonObject("foo", new JsonObject("bar", new JsonObject(new JsonObject[] { true, 4 })));
            File.WriteAllText("object.json", jObj.ToJsonString());
        }

        private static void FromJsonBool() {
            string s = @"true";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Bool);
            Assert.AreEqual(true, jObj.BoolValue);
        }

        private static void FromJsonInt() {
            string s = @"789";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(789, jObj.IntValue);
        }

        private static void FromJsonFloat() {
            string s = @"3.25";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(3.25, jObj.FloatValue);
        }

        private static void FromJsonNaN() {
            var s = "NaN";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(float.NaN, jObj.FloatValue);
        }

        private static void FromJsonInfinity() {
            var s = "Infinity";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(float.PositiveInfinity, jObj.FloatValue);

            s = "-Infinity";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(float.NegativeInfinity, jObj.FloatValue);
        }

        private static void FromJsonString() {
            string s = @"""foo""";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.String);
            Assert.AreEqual("foo", jObj.StringValue);
        }

        private static void FromJsonArray() {
            string s = " [  3.25  ,true  , \"bl a\" ]";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Array);
            Assert.IsTrue(jObj.Count != null);
            Assert.AreEqual(3, jObj.Count.Value);
        }

        private static void FromJsonObject() {
            string s = " { \"foo\" : false , \"bar\" : NaN } ";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Object);
            Assert.IsTrue(jObj.Count != null);
            Assert.AreEqual(2, jObj.Count.Value);
        }

        private static void FromJsonWhitespace() {
            string s = " \n\t98465\r \t";
            jObj = JsonObject.FromJson(s);
            Assert.IsTrue(jObj.Type == JsonObject.EType.Number);
            Assert.AreEqual(98465, jObj.IntValue);
        }

        private static void ToJsonBool() {
            var s = JsonObject.ToJson(true);
            Assert.AreEqual("true", s);
        }

        private static void ToJsonInt() {
            var s = JsonObject.ToJson(3);
            Assert.AreEqual("3", s);
        }

        private static void ToJsonFloat() {
            var s = JsonObject.ToJson(3.256f);
            Assert.AreEqual("3.256", s);
        }

        private static void ToJsonString() {
            var s = JsonObject.ToJson("foo");
            Assert.AreEqual("\"foo\"", s);
        }

        private static void ToJsonArray() {
            var s = JsonObject.ToJson(new JsonObject(new List<JsonObject> {"foo", 3.21f, true}));
            Assert.AreEqual(@"[""foo"",3.21,true]", s);
        }

        private static void ToJsonObject() {
            var s = JsonObject.ToJson(new JsonObject(new Dictionary<string, JsonObject> {{"foo", 3}, {"bar", false}}));
            Assert.AreEqual(@"{""foo"":3,""bar"":false}", s);
        }

        private static void Redeserialize() {
            string expected = @"{""test"":true,""foo"":[[],{},[{""foo"":""myname""},true,Infinity,NaN]],""bar"":{""name"":[]}}";
            string s = @" { ""test"" : true , ""foo"": [ [ ] , { } , [  { ""foo"" : ""myname"" } , true , Infinity , NaN ] ] , ""bar"" : { ""name"" : [ ] } } ";
            jObj = JsonObject.FromJson(s);
            var output = JsonObject.ToJson(jObj);
            Assert.AreEqual(expected, output);
        }
    }

    /// <summary>
    /// Tests the <see cref="LimitedType"/> class
    /// </summary>
    private struct LimitedTypeTest {
        private static void Assign() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            obj.Assign(3);
            obj.Assign(3.5);
            obj.Assign(new List<int>());
            obj.Assign(new Dictionary<int, int>());
            obj.Assign(new Dictionary<int, string>());
            obj.Assign(new Dictionary<string, string>());

            try {
                obj.Assign(3.5f);
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }
            try {
                obj.Assign(new List<double>());
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }
            try {
                obj.Assign(new List<string>());
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }
        }

        private static void Get1() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            obj.Assign(3);
            Assert.AreEqual(3, obj.Get<int>());
            try {
                obj.Get<double>();
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }
        }

        private static void Get2() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            obj.Assign(3.5);
            Assert.AreEqual(3.5, obj.Get<double>());
            try {
                obj.Get<int>();
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }
        }

        private static void Get3() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            var list = new List<int> { 3, 5, 6 };
            obj.Assign(list);
            Assert.AreEqual(list, obj.Get<List<int>>());
        }

        private static void Get4() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            var dInt = new Dictionary<int, int> { { 1, 2 }, { 3, 4 }, { 4, 5 } };
            obj.Assign(dInt);
            Assert.AreEqual(dInt, obj.Get<Dictionary<int, int>>());
        }

        private static void Get5() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            var dStr = new Dictionary<string, string> { { "foo", "bar" }, { "bar", "foo" } };
            obj.Assign(dStr);
            Assert.AreEqual(dStr, obj.Get<Dictionary<string, string>>());
        }

        private static void Null() {
            var obj = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>), typeof(Dictionary<,>) });
            obj.Assign(null);
            Assert.IsNull(obj.Get<List<int>>());
            var temp = new LimitedType(new[] { typeof(int), typeof(double) });
            try {
                temp.Assign(null);
                throw new Exception();
            }
            catch (ArgumentNullException) { }
            try {
                temp.Get<int>();
                throw new Exception();
            }
            catch (NullReferenceException) { }
            temp = new LimitedType(new[] { typeof(int), typeof(double), typeof(List<int>) }) { AllowNull = false };
            try {
                temp.Assign(null);
                throw new Exception();
            }
            catch (ArgumentNullException) { }
        }

        class Base {}

        class Child : Base {}

        private static void GetBase() {
            var temp = new LimitedType(new[] { typeof(Child) });
            temp.Assign(new Child());
            temp.Get<Base>();
            temp.Assign(new Base());
            try {
                temp.Get<Child>();
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }

            temp = new LimitedType(new[] { typeof(Child) });
            temp.Assign(new Child());
            temp.Get<Base>();
            Base b = new Child();
            temp.Assign(b);
            temp.Get<Child>();
        }

        private static void GetForbidBase() {
            var obj = new LimitedType(new[] { typeof(Child) }) { AllowBaseTypes = false };
            obj.Assign(new Child());
            obj.Get<Base>();
            try {
                obj.Assign(new Base());
                throw new Exception();
            }
            catch (InvalidLimitedTypeException) { }
        }

        private static void GetInherit() {
            var obj = new LimitedType(new[] { typeof(Base) });
            obj.Assign(new Child());
        }

        private static void IsCurrent() {
            var obj = new LimitedType(new[] { typeof(int), typeof(bool) });
            obj.Assign(5);
            Assert.IsFalse(obj.IsCurrentType<bool>());
            obj.Assign(true);
            Assert.IsFalse(obj.IsCurrentType<int>());
            obj = new LimitedType(new[] { typeof(Child) });
            obj.Assign(new Child());
            Assert.IsTrue(obj.IsCurrentType<Base>());
            obj.AllowBaseTypes = false;
            Assert.IsTrue(obj.IsCurrentType<Base>());
            obj = new LimitedType(new[] { typeof(int), typeof(long) });
            obj.Assign(5L);
            Assert.IsFalse(obj.IsCurrentType<int>());
            obj.Assign(5);
            Assert.IsFalse(obj.IsCurrentType<long>());
        }
    }

}
