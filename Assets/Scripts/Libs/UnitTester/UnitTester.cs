﻿using UnityEngine;

/// <summary>
/// Tests all sorts of classes and functions.
/// 
/// Usage:
/// In UnitTestEditor, add a struct containing test functions declared as static(!). Add the exact
/// name of the struct then to the enum <see cref="Tests"/> here. In the editor, select the test
/// you want to run (or All) and click Run Unit Tests. The console gives the info you need.
/// </summary>
public class UnitTester : MonoBehaviour {
    [Header("Input values")]
    public bool Bool;
    public int Int;
    public float Float;
    public string String;

    /// <summary>
    /// Name of the test structs.
    /// </summary>
    public enum Tests {
        All,
        QuickTest,
        LimitedTypeTest,
        JsonObjectTest
    }
    /// <summary>
    /// Which test to run
    /// </summary>
    [Header("Run test")]
    public Tests TestsToRun = Tests.All;

    public bool StopOnError = false;
}