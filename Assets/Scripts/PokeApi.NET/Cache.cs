﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PokeApi {
    public class Cache : Web.Cache {

        #region Constructor
        public Cache(bool initLoad =true) : base(false) {
            _cacheDir = "";
            _cacheFile = "PokeApi.cache";
            if (initLoad)
                Load();
        }

        public Cache(string cache) : base(cache) {}
        #endregion

        #region Methods
        /// <summary>
        /// Gets all pages from a paginated page
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerator GetAllNames<T>() {
            var result = new List<NamedAPIResource<T>>();
            string pageName = typeof(T).Name.ToLower();
            NamedAPIResourceList list;
            int count = 0;
            do {
                yield return GetJson(pageName);
                list = JsonUtility.FromJson<NamedAPIResourceList>(LastJson);
                foreach (var resource in list.Results)
                    result.Add(resource);
                pageName = list.Next;
            } while (!string.IsNullOrEmpty(list.Next));
            yield return result;
        }
        #endregion
    }
}
