#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public class APIResource {
        #region Fields

        [SerializeField] protected string url;

        #endregion

        #region Properties

        /// <summary>
        /// The URL of the referenced resource
        /// </summary>
        public string Url {
            get { return url; }
        }

        #endregion
    }

    public class APIResource<T> : APIResource {
        public T Get {
            //TODO implement
            get { throw new NotImplementedException(); }
        }
    }

    #region Workaround

    [Serializable]
    public class APIResourceEvolutionChain : APIResource<EvolutionChain> {}
    [Serializable]
    public class APIResourceMachine : APIResource<Machine> { }

    [Serializable]
    public class APIResourceContestEffect : APIResource<ContestEffect> {}

    [Serializable]
    public class APIResourceSuperContestEffect : APIResource<SuperContestEffect> {}

    [Serializable]
    public class APIResourceCharacteristic : APIResource<Characteristic> {}

    #endregion
}