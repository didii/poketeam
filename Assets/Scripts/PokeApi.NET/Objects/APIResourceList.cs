#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public class APIResourceList {

        #region Fields

        [SerializeField] private int count;
        [SerializeField] private string next;
        [SerializeField] private bool previous;
        [SerializeField] private List<APIResource> results;

        #endregion

        #region Properties

        /// <summary>
        /// The total number of resources available from this API
        /// </summary>
        public int Count {
            get { return count; }
        }

        /// <summary>
        /// The URL for the next page in the list
        /// </summary>
        public string Next {
            get { return next; }
        }

        /// <summary>
        /// The URL for the previous page in the list
        /// </summary>
        public bool Previous {
            get { return previous; }
        }

        /// <summary>
        /// A list of unnamed API resources
        /// </summary>
        public List<APIResource> Results {
            get { return results; }
        }

        #endregion
    }
    //TODO add APIResourceList<T>
}