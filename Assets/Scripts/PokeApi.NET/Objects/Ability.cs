#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Ability {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private bool is_main_series;
        [SerializeField] private NamedAPIResourceGeneration generation;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<VerboseEffect> effect_entries;
        [SerializeField] private List<AbilityEffectChange> effect_changes;
        [SerializeField] private List<AbilityFlavorText> flavor_text_entries;
        [SerializeField] private List<AbilityPokemon> pokemon;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this ability resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this ability resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// Whether or not this ability originated in the main series of the video games
        /// </summary>
        public bool IsMainSeries {
            get { return is_main_series; }
        }

        /// <summary>
        /// The generation this ability originated in
        /// </summary>
        public NamedAPIResourceGeneration Generation {
            get { return generation; }
        }

        /// <summary>
        /// The name of this ability listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// The effect of this ability listed in different languages
        /// </summary>
        public List<VerboseEffect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// The list of previous effects this ability has had across version groups
        /// </summary>
        public List<AbilityEffectChange> EffectChanges {
            get { return effect_changes; }
        }

        /// <summary>
        /// The flavor text of this ability listed in different languages
        /// </summary>
        public List<AbilityFlavorText> FlavorTextEntries {
            get { return flavor_text_entries; }
        }

        /// <summary>
        /// A list of Pokémon that could potentially have this ability
        /// </summary>
        public List<AbilityPokemon> Pokemon {
            get { return pokemon; }
        }

        #endregion
    }
}