#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct AbilityEffectChange {
        #region Fields

        [SerializeField] private List<Effect> effect_entries;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties

        /// <summary>
        /// The previous effect of this ability listed in different languages
        /// </summary>
        public List<Effect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// The version group in which the previous effect of this ability originated
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}