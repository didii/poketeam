﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct AbilityPokemon {
        #region Fields

        [SerializeField] private bool is_hidden;
        [SerializeField] private int slot;
        [SerializeField] private NamedAPIResourcePokemon pokemon;

        #endregion

        #region Properties

        /// <summary>
        /// Whether or not this a hidden ability for the referenced Pokémon
        /// </summary>
        public bool IsHidden {
            get { return is_hidden; }
        }

        /// <summary>
        /// Pokémon have 3 ability 'slots' which hold references to possible abilities they could have. This is the slot of this ability for the referenced pokemon.
        /// </summary>
        public int Slot {
            get { return slot; }
        }

        /// <summary>
        /// The Pokémon this ability could belong to
        /// </summary>
        public NamedAPIResourcePokemon Pokemon {
            get { return pokemon; }
        }

        #endregion
    }
}