#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct AwesomeName {
        #region Fields

        [SerializeField] private string awesome_name;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The localized "scientific" name for an API resource in a specific language
        /// </summary>
        public string Name {
            get { return awesome_name; }
        }

        /// <summary>
        /// The language this "scientific" name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}