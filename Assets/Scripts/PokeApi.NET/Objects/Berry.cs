﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Berry {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int growth_time;
        [SerializeField] private int max_harvest;
        [SerializeField] private int natural_gift_power;
        [SerializeField] private int size;
        [SerializeField] private int smoothness;
        [SerializeField] private int soil_dryness;
        [SerializeField] private NamedAPIResourceBerryFirmness firmness;
        [SerializeField] private List<BerryFlavorMap> flavors;
        [SerializeField] private NamedAPIResourceItem item;
        [SerializeField] private NamedAPIResourceType natural_gift_type;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this berry resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this berry resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// Time it takes the tree to grow one stage, in hours. Berry trees go through four of these growth stages before they can be picked.
        /// </summary>
        public int GrowthTime {
            get { return growth_time; }
        }

        /// <summary>
        /// The maximum number of these berries that can grow on one tree in Generation IV
        /// </summary>
        public int MaxHarvest {
            get { return max_harvest; }
        }

        /// <summary>
        /// The power of the move "Natural Gift" when used with this Berry
        /// </summary>
        public int NaturalGiftPower {
            get { return natural_gift_power; }
        }

        /// <summary>
        /// The size of this Berry, in millimeters
        /// </summary>
        public int Size {
            get { return size; }
        }

        /// <summary>
        /// The smoothness of this Berry, used in making Pokéblocks or Poffins
        /// </summary>
        public int Smoothness {
            get { return smoothness; }
        }

        /// <summary>
        /// The speed at which this Berry dries out the soil as it grows. A higher rate means the soil dries more quickly.
        /// </summary>
        public int SoilDryness {
            get { return soil_dryness; }
        }

        /// <summary>
        /// The firmness of this berry, used in making Pokéblocks or Poffins
        /// </summary>
        public NamedAPIResourceBerryFirmness Firmness {
            get { return firmness; }
        }

        /// <summary>
        /// A list of references to each flavor a berry can have and the potency of each of those flavors in regard to this berry
        /// </summary>
        public List<BerryFlavorMap> Flavors {
            get { return flavors; }
        }

        /// <summary>
        /// Berries are actually items. This is a reference to the item specific data for this berry.
        /// </summary>
        public NamedAPIResourceItem Item {
            get { return item; }
        }

        /// <summary>
        /// The Type the move "Natural Gift" has when used with this Berry
        /// </summary>
        public NamedAPIResourceType NaturalGiftType {
            get { return natural_gift_type; }
        }

        #endregion
    }
}