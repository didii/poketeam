#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct BerryFirmness {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceBerry> berries;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this berry firmness resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this berry firmness resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of the berries with this firmness
        /// </summary>
        public List<NamedAPIResourceBerry> Berries {
            get { return berries; }
        }

        /// <summary>
        /// The name of this berry firmness listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}