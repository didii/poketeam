#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct BerryFlavor {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<FlavorBerryMap> berries;
        [SerializeField] private NamedAPIResourceContestType contest_type;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this berry flavor resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this berry flavor resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of the berries with this flavor
        /// </summary>
        public List<FlavorBerryMap> Berries {
            get { return berries; }
        }

        /// <summary>
        /// The contest type that correlates with this berry flavor
        /// </summary>
        public NamedAPIResourceContestType ContestType {
            get { return contest_type; }
        }

        /// <summary>
        /// The name of this berry flavor listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}