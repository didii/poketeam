#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct BerryFlavorMap {
        #region Fields

        [SerializeField] private int potency;
        [SerializeField] private NamedAPIResourceBerryFlavor flavor;

        #endregion

        #region Properties

        /// <summary>
        /// How powerful the referenced flavor is for this berry
        /// </summary>
        public int Potency {
            get { return potency; }
        }

        /// <summary>
        /// The referenced berry flavor
        /// </summary>
        public NamedAPIResourceBerryFlavor Flavor {
            get { return flavor; }
        }

        #endregion
    }
}