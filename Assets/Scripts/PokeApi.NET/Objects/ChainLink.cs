﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public class ChainLink {
        #region Fields

        [SerializeField] private bool is_baby;
        [SerializeField] private NamedAPIResourcePokemonSpecies species;
        [SerializeField] private List<EvolutionDetail> evolution_details;
        [SerializeField] private List<ChainLink> evolves_to;

        #endregion

        #region Properties

        /// <summary>
        /// Whether or not this link is for a baby Pokémon. This would only ever be true on the base link.
        /// </summary>
        public bool IsBaby {
            get { return is_baby; }
        }

        /// <summary>
        /// The Pokémon species at this point in the evolution chain
        /// </summary>
        public NamedAPIResourcePokemonSpecies Species {
            get { return species; }
        }

        /// <summary>
        /// All details regarding the specific details of the referenced Pokémon species evolution
        /// </summary>
        public List<EvolutionDetail> EvolutionDetails {
            get { return evolution_details; }
        }

        /// <summary>
        /// A List of chain objects.
        /// </summary>
        public List<ChainLink> EvolvesTo {
            get { return evolves_to; }
        }

        #endregion
    }
}