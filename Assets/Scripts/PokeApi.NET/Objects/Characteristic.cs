﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Characteristic {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private int gene_modulo;
        [SerializeField] private List<int> possible_values;
        [SerializeField] private List<Description> descriptions;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this characteristic resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The remainder of the highest stat/IV divided by 5
        /// </summary>
        public int GeneModulo {
            get { return gene_modulo; }
        }

        /// <summary>
        /// The possible values of the highest stat that would result in a Pokémon recieving this characteristic when divided by 5
        /// </summary>
        public List<int> PossibleValues {
            get
            {
                return possible_values;
            }
        }

        /// <summary>
        /// The descriptions of this characteristic listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        #endregion
    }
}