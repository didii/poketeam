#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ContestComboDetail {
        #region Fields

        [SerializeField] private List<NamedAPIResourceMove> use_before;
        [SerializeField] private List<NamedAPIResourceMove> use_after;

        #endregion

        #region Properties

        /// <summary>
        /// A list of moves to use before this move
        /// </summary>
        public List<NamedAPIResourceMove> UseBefore {
            get { return use_before; }
        }

        /// <summary>
        /// A list of moves to use after this move
        /// </summary>
        public List<NamedAPIResourceMove> UseAfter {
            get { return use_after; }
        }

        #endregion
    }
}