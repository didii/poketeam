#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ContestComboSets {
        #region Fields

        [SerializeField] private ContestComboDetail normal;
        [SerializeField] private ContestComboDetail super;

        #endregion

        #region Properties

        /// <summary>
        /// A detail of moves this move can be used before or after, granting additional appeal points in contests
        /// </summary>
        public ContestComboDetail Normal {
            get { return normal; }
        }

        /// <summary>
        /// A detail of moves this move can be used before or after, granting additional appeal points in super contests
        /// </summary>
        public ContestComboDetail Super {
            get { return super; }
        }

        #endregion
    }
}