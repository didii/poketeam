#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ContestEffect {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private int appeal;
        [SerializeField] private int jam;
        [SerializeField] private List<Effect> effect_entries;
        [SerializeField] private List<FlavorText> flavor_text_entries;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this contest type resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The base number of hearts the user of this move gets
        /// </summary>
        public int Appeal {
            get { return appeal; }
        }

        /// <summary>
        /// The base number of hearts the user's opponent loses
        /// </summary>
        public int Jam {
            get { return jam; }
        }

        /// <summary>
        /// The result of this contest effect listed in different languages
        /// </summary>
        public List<Effect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// The flavor text of this contest effect listed in different languages
        /// </summary>
        public List<FlavorText> FlavorTextEntries {
            get { return flavor_text_entries; }
        }

        #endregion
    }
}