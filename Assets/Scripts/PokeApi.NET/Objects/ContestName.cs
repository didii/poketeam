#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ContestName {
        #region Fields

        [SerializeField] private string name;
        [SerializeField] private string color;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The name for this contest
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The color associated with this contest's name
        /// </summary>
        public string Color {
            get { return color; }
        }

        /// <summary>
        /// The language that this name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}