#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ContestType {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private NamedAPIResourceBerryFlavor berry_flavor;
        [SerializeField] private List<ContestName> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this contest type resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this contest type resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The berry flavor that correlates with this contest type
        /// </summary>
        public NamedAPIResourceBerryFlavor BerryFlavor {
            get { return berry_flavor; }
        }

        /// <summary>
        /// The name of this contest type listed in different languages
        /// </summary>
        public List<ContestName> Names {
            get { return names; }
        }

        #endregion
    }
}