#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Description {
        #region Fields

        [SerializeField] private string description;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The localized description for an API resource in a specific language
        /// </summary>
        public string Text {
            get { return description; }
        }

        /// <summary>
        /// The language this name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}