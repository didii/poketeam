﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Encounter {
        #region Fields

        [SerializeField] private int min_level;
        [SerializeField] private int max_level;
        [SerializeField] private List<NamedAPIResourceEncounterConditionValue> condition_values;
        [SerializeField] private int chance;
        [SerializeField] private NamedAPIResourceEncounterMethod method;

        #endregion

        #region Properties

        /// <summary>
        /// The lowest level the Pokémon could be encountered at
        /// </summary>
        public int MinLevel {
            get { return min_level; }
        }

        /// <summary>
        /// The highest level the Pokémon could be encountered at
        /// </summary>
        public int MaxLevel {
            get { return max_level; }
        }

        /// <summary>
        /// A list of condition values that must be in effect for this encounter to occur
        /// </summary>
        public List<NamedAPIResourceEncounterConditionValue> ConditionValues {
            get { return condition_values; }
        }

        /// <summary>
        /// percent chance that this encounter will occur
        /// </summary>
        public int Chance {
            get { return chance; }
        }

        /// <summary>
        /// The method by which this encounter happens
        /// </summary>
        public NamedAPIResourceEncounterMethod Method {
            get { return method; }
        }

        #endregion
    }
}