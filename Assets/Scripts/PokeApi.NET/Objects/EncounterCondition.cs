#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EncounterCondition {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<NamedAPIResourceEncounterConditionValue> values;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this encounter condition resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this encounter condition resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The name of this encounter method listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of possible values for this encounter condition
        /// </summary>
        public List<NamedAPIResourceEncounterConditionValue> Values {
            get { return values; }
        }

        #endregion
    }
}