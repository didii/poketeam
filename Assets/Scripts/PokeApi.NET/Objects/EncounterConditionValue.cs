#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EncounterConditionValue {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceEncounterCondition> condition;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this encounter condition value resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this encounter condition value resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The condition this encounter condition value pertains to
        /// </summary>
        public List<NamedAPIResourceEncounterCondition> Condition {
            get { return condition; }
        }

        /// <summary>
        /// The name of this encounter condition value listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}