#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EncounterMethod {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int order;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this encounter method resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this encounter method resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A good value for sorting
        /// </summary>
        public int Order {
            get { return order; }
        }

        /// <summary>
        /// The name of this encounter method listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}