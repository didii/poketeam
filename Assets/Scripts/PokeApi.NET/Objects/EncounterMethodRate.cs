﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EncounterMethodRate {
        #region Fields

        [SerializeField] private NamedAPIResourceEncounterMethod encounter_method;
        [SerializeField] private List<EncounterVersionDetails> version_details;

        #endregion

        #region Properties

        /// <summary>
        /// The method in which Pokémon may be encountered in an area.
        /// </summary>
        public NamedAPIResourceEncounterMethod EncounterMethod {
            get { return encounter_method; }
        }

        /// <summary>
        /// The chance of the encounter to occur on a version of the game.
        /// </summary>
        public List<EncounterVersionDetails> VersionDetails {
            get { return version_details; }
        }

        #endregion
    }
}