#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EncounterVersionDetails {
        #region Fields

        [SerializeField] private int rate;
        [SerializeField] private NamedAPIResourceVersion version;

        #endregion

        #region Properties

        /// <summary>
        /// The chance of an encounter to occur.
        /// </summary>
        public int Rate {
            get { return rate; }
        }

        /// <summary>
        /// The version of the game in which the encounter can occur with the given chance.
        /// </summary>
        public NamedAPIResourceVersion Version {
            get { return version; }
        }

        #endregion
    }
}