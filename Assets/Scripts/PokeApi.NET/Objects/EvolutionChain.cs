﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EvolutionChain {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private NamedAPIResourceItem baby_trigger_item;
        [SerializeField] private ChainLink chain;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this evolution chain resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The item that a Pokémon would be holding when mating that would trigger the egg hatching a baby Pokémon rather than a basic Pokémon
        /// </summary>
        public NamedAPIResourceItem BabyTriggerItem {
            get { return baby_trigger_item; }
        }

        /// <summary>
        /// The base chain link object. Each link contains evolution details for a Pokémon in the chain. Each link references the next Pokémon in the natural evolution order.
        /// </summary>
        public ChainLink Chain {
            get { return chain; }
        }

        #endregion
    }
}