﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EvolutionDetail {
        #region Fields

        [SerializeField] private NamedAPIResourceItem item;
        [SerializeField] private NamedAPIResourceEvolutionTrigger trigger;
        [SerializeField] private int gender;
        [SerializeField] private NamedAPIResourceItem held_item;
        [SerializeField] private NamedAPIResourceMove known_move;
        [SerializeField] private NamedAPIResourceType known_move_type;
        [SerializeField] private NamedAPIResourceLocation location;
        [SerializeField] private int min_level;
        [SerializeField] private int min_happiness;
        [SerializeField] private int min_beauty;
        [SerializeField] private int min_affection;
        [SerializeField] private bool needs_overworld_rain;
        [SerializeField] private NamedAPIResourcePokemonSpecies party_species;
        [SerializeField] private NamedAPIResourceType party_type;
        [SerializeField] private int relative_physical_stats;
        [SerializeField] private string time_of_day;
        [SerializeField] private NamedAPIResourcePokemonSpecies trade_species;
        [SerializeField] private bool turn_upside_down;

        #endregion

        #region Properties

        /// <summary>
        /// The item required to cause evolution this into Pokémon species
        /// </summary>
        public NamedAPIResourceItem Item {
            get { return item; }
        }

        /// <summary>
        /// The type of event that triggers evolution into this Pokémon species
        /// </summary>
        public NamedAPIResourceEvolutionTrigger Trigger {
            get { return trigger; }
        }

        /// <summary>
        /// The id of the gender of the evolving Pokémon species must be in order to evolve into this Pokémon species
        /// </summary>
        public int Gender {
            get { return gender; }
        }

        /// <summary>
        /// The item the evolving Pokémon species must be holding during the evolution trigger event to evolve into this Pokémon species
        /// </summary>
        public NamedAPIResourceItem HeldItem {
            get { return held_item; }
        }

        /// <summary>
        /// The move that must be known by the evolving Pokémon species during the evolution trigger event in order to evolve into this Pokémon species
        /// </summary>
        public NamedAPIResourceMove KnownMove {
            get { return known_move; }
        }

        /// <summary>
        /// The evolving Pokémon species must know a move with this type during the evolution trigger event in order to evolve into this Pokémon species
        /// </summary>
        public NamedAPIResourceType KnownMoveType {
            get { return known_move_type; }
        }

        /// <summary>
        /// The location the evolution must be triggered at.
        /// </summary>
        public NamedAPIResourceLocation Location {
            get { return location; }
        }

        /// <summary>
        /// The minimum required level of the evolving Pokémon species to evolve into this Pokémon species
        /// </summary>
        public int MinLevel {
            get { return min_level; }
        }

        /// <summary>
        /// The minimum required level of happiness the evolving Pokémon species to evolve into this Pokémon species
        /// </summary>
        public int MinHappiness {
            get { return min_happiness; }
        }

        /// <summary>
        /// The minimum required level of beauty the evolving Pokémon species to evolve into this Pokémon species
        /// </summary>
        public int MinBeauty {
            get { return min_beauty; }
        }

        /// <summary>
        /// The minimum required level of affection the evolving Pokémon species to evolve into this Pokémon species
        /// </summary>
        public int MinAffection {
            get { return min_affection; }
        }

        /// <summary>
        /// Whether or not it must be raining in the overworld to cause evolution this Pokémon species
        /// </summary>
        public bool NeedsOverworldRain {
            get { return needs_overworld_rain; }
        }

        /// <summary>
        /// The Pokémon species that must be in the players party in order for the evolving Pokémon species to evolve into this Pokémon species
        /// </summary>
        public NamedAPIResourcePokemonSpecies PartySpecies {
            get { return party_species; }
        }

        /// <summary>
        /// The player must have a Pokémon of this type in their party during the evolution trigger event in order for the evolving Pokémon species to evolve into this Pokémon species
        /// </summary>
        public NamedAPIResourceType PartyType {
            get { return party_type; }
        }

        /// <summary>
        /// The required relation between the Pokémon's Attack and Defense stats. 1 means Attack > Defense. 0 means Attack = Defense. -1 means Attack < Defense.
        /// </summary>
        public int RelativePhysicalStats {
            get { return relative_physical_stats; }
        }

        /// <summary>
        /// The required time of day. Day or night.
        /// </summary>
        public string TimeOfDay {
            get { return time_of_day; }
        }

        /// <summary>
        /// Pokémon species for which this one must be traded.
        /// </summary>
        public NamedAPIResourcePokemonSpecies TradeSpecies {
            get { return trade_species; }
        }

        /// <summary>
        /// Whether or not the 3DS needs to be turned upside-down as this Pokémon levels up.
        /// </summary>
        public bool TurnUpsideDown {
            get { return turn_upside_down; }
        }

        #endregion
    }
}