#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct EvolutionTrigger {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<NamedAPIResourcePokemonSpecies> pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this evolution trigger resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this evolution trigger resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The name of this evolution trigger listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of pokemon species that result from this evolution trigger
        /// </summary>
        public List<NamedAPIResourcePokemonSpecies> PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}