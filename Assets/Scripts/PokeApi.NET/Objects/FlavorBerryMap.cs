#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct FlavorBerryMap {
        #region Fields

        [SerializeField] private int potency;
        [SerializeField] private NamedAPIResourceBerry berry;

        #endregion

        #region Properties

        /// <summary>
        /// How powerful the referenced flavor is for this berry
        /// </summary>
        public int Potency {
            get { return potency; }
        }

        /// <summary>
        /// The berry with the referenced flavor
        /// </summary>
        public NamedAPIResourceBerry Berry {
            get { return berry; }
        }

        #endregion
    }
}