#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct FlavorText {
        #region Fields

        [SerializeField] private string flavor_text;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The localized flavor text for an API resource in a specific language
        /// </summary>
        public string Text {
            get { return flavor_text; }
        }

        /// <summary>
        /// The language this name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}