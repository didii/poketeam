﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Gender {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<PokemonSpeciesGender> pokemon_species_details;
        [SerializeField] private List<NamedAPIResourcePokemonSpecies> required_for_evolution;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this gender resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this gender resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of Pokémon species that can be this gender and how likely it is that they will be
        /// </summary>
        public List<PokemonSpeciesGender> PokemonSpeciesDetails {
            get { return pokemon_species_details; }
        }

        /// <summary>
        /// A list of Pokémon species that required this gender in order for a Pokémon to evolve into them
        /// </summary>
        public List<NamedAPIResourcePokemonSpecies> RequiredForEvolution {
            get { return required_for_evolution; }
        }

        #endregion
    }
}