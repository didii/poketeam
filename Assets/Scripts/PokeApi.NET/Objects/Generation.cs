﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Generation {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceAbility> abilities;
        [SerializeField] private List<Name> names;
        [SerializeField] private NamedAPIResourceRegion main_region;
        [SerializeField] private List<NamedAPIResourceMove> moves;
        [SerializeField] private List<NamedAPIResourcePokemonSpecies> pokemon_species;
        [SerializeField] private List<NamedAPIResourceType> types;
        [SerializeField] private List<NamedAPIResourceVersionGroup> version_groups;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this generation resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this generation resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of abilities that were introduced in this generation
        /// </summary>
        public List<NamedAPIResourceAbility> Abilities {
            get { return abilities; }
        }

        /// <summary>
        /// The name of this generation listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// The main region travelled in this generation
        /// </summary>
        public NamedAPIResourceRegion MainRegion {
            get { return main_region; }
        }

        /// <summary>
        /// A list of moves that were introduced in this generation
        /// </summary>
        public List<NamedAPIResourceMove> Moves {
            get { return moves; }
        }

        /// <summary>
        /// A list of Pokémon species that were introduced in this generation
        /// </summary>
        public List<NamedAPIResourcePokemonSpecies> PokemonSpecies {
            get { return pokemon_species; }
        }

        /// <summary>
        /// A list of types that were introduced in this generation
        /// </summary>
        public List<NamedAPIResourceType> Types {
            get { return types; }
        }

        /// <summary>
        /// A list of version groups that were introduced in this generation
        /// </summary>
        public List<NamedAPIResourceVersionGroup> VersionGroups {
            get { return version_groups; }
        }

        #endregion
    }
}