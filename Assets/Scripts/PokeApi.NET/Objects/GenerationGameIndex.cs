#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct GenerationGameIndex {
        #region Fields

        [SerializeField] private int game_index;
        [SerializeField] private NamedAPIResourceGeneration generation;

        #endregion

        #region Properties

        /// <summary>
        /// The internal id of an API resource within game data
        /// </summary>
        public int GameIndex {
            get { return game_index; }
        }

        /// <summary>
        /// The generation relevent to this game index
        /// </summary>
        public NamedAPIResourceGeneration Generation {
            get { return generation; }
        }

        #endregion
    }
}