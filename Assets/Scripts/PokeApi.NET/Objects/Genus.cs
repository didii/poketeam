﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Genus {
        #region Fields

        [SerializeField] private string genus;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The localized genus for the referenced Pokémon species
        /// </summary>
        public string Name {
            get { return genus; }
        }

        /// <summary>
        /// The language this genus is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}