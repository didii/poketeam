﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct GrowthRate {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private string formula;
        [SerializeField] private List<Description> descriptions;
        [SerializeField] private List<GrowthRateExperienceLevel> levels;
        [SerializeField] private List<NamedAPIResourcePokemonSpecies> pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this gender resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this gender resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The formula used to calculate the rate at which the Pokémon species gains level
        /// </summary>
        public string Formula {
            get { return formula; }
        }

        /// <summary>
        /// The descriptions of this characteristic listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        /// <summary>
        /// A list of levels and the amount of experienced needed to atain them based on this growth rate
        /// </summary>
        public List<GrowthRateExperienceLevel> Levels {
            get { return levels; }
        }

        /// <summary>
        /// A list of Pokémon species that gain levels at this growth rate
        /// </summary>
        public List<NamedAPIResourcePokemonSpecies> PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}