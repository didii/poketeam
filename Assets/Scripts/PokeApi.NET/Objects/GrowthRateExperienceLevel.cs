#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct GrowthRateExperienceLevel {
        #region Fields

        [SerializeField] private int level;
        [SerializeField] private int experience;

        #endregion

        #region Properties

        /// <summary>
        /// The level gained
        /// </summary>
        public int Level {
            get { return level; }
        }

        /// <summary>
        /// The amount of experience required to reach the referenced level
        /// </summary>
        public int Experience {
            get { return experience; }
        }

        #endregion
    }
}