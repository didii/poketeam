﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Item {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int cost;
        [SerializeField] private int fling_power;
        [SerializeField] private NamedAPIResourceItemFlingEffect fling_effect;
        [SerializeField] private List<NamedAPIResourceItemAttribute> attributes;
        [SerializeField] private ItemCategory category;
        [SerializeField] private List<VerboseEffect> effect_entries;
        [SerializeField] private List<VersionGroupFlavorText> flavor_text_entries;
        [SerializeField] private List<GenerationGameIndex> game_indices;
        [SerializeField] private List<Name> names;
        [SerializeField] private ItemSprites sprites;
        [SerializeField] private List<ItemHolderPokemon> held_by_pokemon;
        [SerializeField] private APIResourceEvolutionChain baby_trigger_for;
        [SerializeField] private List<MachineVersionDetail> machines;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this item resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this item resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The price of this item in stores
        /// </summary>
        public int Cost {
            get { return cost; }
        }

        /// <summary>
        /// The power of the move Fling when used with this item.
        /// </summary>
        public int FlingPower {
            get { return fling_power; }
        }

        /// <summary>
        /// The effect of the move Fling when used with this item
        /// </summary>
        public NamedAPIResourceItemFlingEffect FlingEffect {
            get { return fling_effect; }
        }

        /// <summary>
        /// A list of attributes this item has
        /// </summary>
        public List<NamedAPIResourceItemAttribute> Attributes {
            get { return attributes; }
        }

        /// <summary>
        /// The category of items this item falls into
        /// </summary>
        public ItemCategory Category {
            get { return category; }
        }

        /// <summary>
        /// The effect of this ability listed in different languages
        /// </summary>
        public List<VerboseEffect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// The flavor text of this ability listed in different languages
        /// </summary>
        public List<VersionGroupFlavorText> FlavorTextEntries {
            get { return flavor_text_entries; }
        }

        /// <summary>
        /// A list of game indices relevent to this item by generation
        /// </summary>
        public List<GenerationGameIndex> GameIndices {
            get { return game_indices; }
        }

        /// <summary>
        /// The name of this item listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A set of sprites used to depict this item in the game
        /// </summary>
        public ItemSprites Sprites {
            get { return sprites; }
        }

        /// <summary>
        /// A list of Pokémon that might be found in the wild holding this item
        /// </summary>
        public List<ItemHolderPokemon> HeldByPokemon {
            get { return held_by_pokemon; }
        }

        /// <summary>
        /// An evolution chain this item requires to produce a bay during mating
        /// </summary>
        public APIResourceEvolutionChain BabyTriggerFor {
            get { return baby_trigger_for; }
        }

        /// <summary>
        /// A list of the machines related to this item
        /// </summary>
        public List<MachineVersionDetail> Machines {
            get { return machines; }
        }

        #endregion
    }
}