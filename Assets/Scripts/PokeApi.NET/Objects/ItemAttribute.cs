#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemAttribute {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceItem> items;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<Description> descriptions;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this item attribute resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this item attribute resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of items that have this attribute
        /// </summary>
        public List<NamedAPIResourceItem> Items {
            get { return items; }
        }

        /// <summary>
        /// The name of this item attribute listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// The description of this item attribute listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        #endregion
    }
}