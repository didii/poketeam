#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemCategory {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceItem> items;
        [SerializeField] private List<Name> names;
        [SerializeField] private NamedAPIResourceItemPocket pocket;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this item category resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this item category resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of items that are a part of this category
        /// </summary>
        public List<NamedAPIResourceItem> Items {
            get { return items; }
        }

        /// <summary>
        /// The name of this item category listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// The pocket items in this category would be put in
        /// </summary>
        public NamedAPIResourceItemPocket Pocket {
            get { return pocket; }
        }

        #endregion
    }
}