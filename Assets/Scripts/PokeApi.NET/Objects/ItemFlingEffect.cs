#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemFlingEffect {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Effect> effect_entries;
        [SerializeField] private List<NamedAPIResourceItem> items;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this fling effect resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this fling effect resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The result of this fling effect listed in different languages
        /// </summary>
        public List<Effect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// A list of items that have this fling effect
        /// </summary>
        public List<NamedAPIResourceItem> Items {
            get { return items; }
        }

        #endregion
    }
}