﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemHolderPokemon {
        #region Fields

        [SerializeField] private string pokemon;
        [SerializeField] private List<ItemHolderPokemonVersionDetail> version_details;

        #endregion

        #region Properties

        /// <summary>
        /// The Pokémon that holds this item
        /// </summary>
        public string Pokemon {
            get { return pokemon; }
        }

        /// <summary>
        /// The details for the version that this item is held in by the Pokémon
        /// </summary>
        public List<ItemHolderPokemonVersionDetail> VersionDetails {
            get { return version_details; }
        }

        #endregion
    }
}