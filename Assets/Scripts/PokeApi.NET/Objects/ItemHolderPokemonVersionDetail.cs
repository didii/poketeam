﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemHolderPokemonVersionDetail {
        #region Fields

        [SerializeField] private string rarity;
        [SerializeField] private NamedAPIResourceVersion version;

        #endregion

        #region Properties

        /// <summary>
        /// How often this Pokémon holds this item in this version
        /// </summary>
        public string Rarity {
            get { return rarity; }
        }

        /// <summary>
        /// The version that this item is held in by the Pokémon
        /// </summary>
        public NamedAPIResourceVersion Version {
            get { return version; }
        }

        #endregion
    }
}