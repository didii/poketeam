#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemPocket {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceItemCategory> categories;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this item pocket resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this item pocket resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of item categories that are relevant to this item pocket
        /// </summary>
        public List<NamedAPIResourceItemCategory> Categories {
            get { return categories; }
        }

        /// <summary>
        /// The name of this item pocket listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}