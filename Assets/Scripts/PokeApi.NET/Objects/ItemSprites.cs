#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct ItemSprites {
        #region Fields
        //TODO: these fields have the wrong name
        [SerializeField]
        private string default_;

        #endregion

        #region Properties

        /// <summary>
        /// The default depiction of this item
        /// </summary>
        public string Default {
            get { return default_; }
        }

        #endregion
    }
}