#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Language {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private bool official;
        [SerializeField] private string iso639;
        [SerializeField] private string iso3166;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this language resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this language resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// Whether or not the games are published in this language
        /// </summary>
        public bool Official {
            get { return official; }
        }

        /// <summary>
        /// The two-letter code of the country where this language is spoken. Note that it is not unique.
        /// </summary>
        public string Iso639 {
            get { return iso639; }
        }

        /// <summary>
        /// The two-letter code of the language. Note that it is not unique.
        /// </summary>
        public string Iso3166 {
            get { return iso3166; }
        }

        /// <summary>
        /// The name of this language listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}