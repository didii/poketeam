#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Location {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private NamedAPIResourceRegion region;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<GenerationGameIndex> game_indices;
        [SerializeField] private List<NamedAPIResourceLocationArea> areas;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this location resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this location resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The region this location can be found in
        /// </summary>
        public NamedAPIResourceRegion Region {
            get { return region; }
        }

        /// <summary>
        /// The name of this language listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of game indices relevent to this location by generation
        /// </summary>
        public List<GenerationGameIndex> GameIndices {
            get { return game_indices; }
        }

        /// <summary>
        /// Areas that can be found within this location
        /// </summary>
        public List<NamedAPIResourceLocationArea> Areas {
            get { return areas; }
        }

        #endregion
    }
}