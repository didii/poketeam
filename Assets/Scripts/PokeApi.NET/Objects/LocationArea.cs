﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct LocationArea {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int game_index;
        [SerializeField] private List<EncounterMethodRate> encounter_method_rates;
        [SerializeField] private NamedAPIResourceRegion location;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<PokemonEncounter> pokemon_encounters;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this location resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this location resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The internal id of an API resource within game data
        /// </summary>
        public int GameIndex {
            get { return game_index; }
        }

        /// <summary>
        /// A list of methods in which Pokémon may be encountered in this area and how likely the method will occur depending on the version of the game
        /// </summary>
        public List<EncounterMethodRate> EncounterMethodRates {
            get { return encounter_method_rates; }
        }

        /// <summary>
        /// The region this location can be found in
        /// </summary>
        public NamedAPIResourceRegion Location {
            get { return location; }
        }

        /// <summary>
        /// The name of this location area listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of Pokémon that can be encountered in this area along with version specific details about the encounter
        /// </summary>
        public List<PokemonEncounter> PokemonEncounters {
            get { return pokemon_encounters; }
        }

        #endregion
    }
}