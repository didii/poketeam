﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct LocationAreaEncounter {
        #region Fields

        [SerializeField] private NamedAPIResourceLocationArea location_area;
        [SerializeField] private List<VersionEncounterDetail> version_details;

        #endregion

        #region Properties

        /// <summary>
        /// The location area the referenced Pokémon can be encountered in
        /// </summary>
        public NamedAPIResourceLocationArea LocationArea {
            get { return location_area; }
        }

        /// <summary>
        /// A list of versions and encounters with the referenced Pokémon that might happen
        /// </summary>
        public List<VersionEncounterDetail> VersionDetails {
            get { return version_details; }
        }

        #endregion
    }
}