﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Machine {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private NamedAPIResourceItem item;
        [SerializeField] private NamedAPIResourceMove move;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties
        /// <summary>
        /// The identifier for this machine resource
        /// </summary>
        public int Id {
            get { return id; }
        }
        /// <summary>
        /// The TM or HM item that corresponds to this machine
        /// </summary>
        public NamedAPIResourceItem Item {
            get { return item; }
        }
        /// <summary>
        /// The move that is taught by this machine
        /// </summary>
        public NamedAPIResourceMove Move {
            get { return move; }
        }
        /// <summary>
        /// The version group that this machine applies to
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}
