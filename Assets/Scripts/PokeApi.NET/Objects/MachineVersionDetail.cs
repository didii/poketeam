#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MachineVersionDetail {
        #region Fields

        [SerializeField] private APIResourceMachine machine;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties

        /// <summary>
        /// The machine that teaches a move from an item
        /// </summary>
        public APIResourceMachine Machine {
            get { return machine; }
        }

        /// <summary>
        /// The version group of this specific machine
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}