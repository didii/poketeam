﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Move {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int accuracy;
        [SerializeField] private int effect_chance;
        [SerializeField] private int pp;
        [SerializeField] private int priority;
        [SerializeField] private int power;
        [SerializeField] private ContestComboSets contest_combos;
        [SerializeField] private NamedAPIResourceContestType contest_type;
        [SerializeField] private APIResourceContestEffect contest_effect;
        [SerializeField] private NamedAPIResourceMoveDamageClass damage_class;
        [SerializeField] private List<VerboseEffect> effect_entries;
        [SerializeField] private List<AbilityEffectChange> effect_changes;
        [SerializeField] private List<MoveFlavorText> flavor_text_entries;
        [SerializeField] private NamedAPIResourceGeneration generation;
        [SerializeField] private List<MachineVersionDetail> machines;
        [SerializeField] private MoveMetaData meta;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<PastMoveStatValues> past_values;
        [SerializeField] private List<MoveStatChange> stat_changes;
        [SerializeField] private APIResourceSuperContestEffect super_contest_effect;
        [SerializeField] private NamedAPIResourceMoveTarget target;
        [SerializeField] private NamedAPIResourceType type;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this move resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this move resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The percent value of how likely this move is to be successful
        /// </summary>
        public int Accuracy {
            get { return accuracy; }
        }

        /// <summary>
        /// The percent value of how likely it is this moves effect will happen
        /// </summary>
        public int EffectChance {
            get { return effect_chance; }
        }

        /// <summary>
        /// Power points. The number of times this move can be used
        /// </summary>
        public int Pp {
            get { return pp; }
        }

        /// <summary>
        /// A value between -8 and 8. Sets the order in which moves are executed during battle. See 
        /// </summary>
        public int Priority {
            get { return priority; }
        }

        /// <summary>
        /// The base power of this move with a value of 0 if it does not have a base power
        /// </summary>
        public int Power {
            get { return power; }
        }

        /// <summary>
        /// A detail of normal and super contest combos that require this move
        /// </summary>
        public ContestComboSets ContestCombos {
            get { return contest_combos; }
        }

        /// <summary>
        /// The type of appeal this move gives a Pokémon when used in a contest
        /// </summary>
        public NamedAPIResourceContestType ContestType {
            get { return contest_type; }
        }

        /// <summary>
        /// The effect the move has when used in a contest
        /// </summary>
        public APIResourceContestEffect ContestEffect {
            get { return contest_effect; }
        }

        /// <summary>
        /// The type of damage the move inflicts on the target, e.g. physical
        /// </summary>
        public NamedAPIResourceMoveDamageClass DamageClass {
            get { return damage_class; }
        }

        /// <summary>
        /// The effect of this move listed in different languages
        /// </summary>
        public List<VerboseEffect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// The list of previous effects this move has had across version groups of the games
        /// </summary>
        public List<AbilityEffectChange> EffectChanges {
            get { return effect_changes; }
        }

        /// <summary>
        /// The flavor text of this move listed in different languages
        /// </summary>
        public List<MoveFlavorText> FlavorTextEntries {
            get { return flavor_text_entries; }
        }

        /// <summary>
        /// The generation in which this move was introduced
        /// </summary>
        public NamedAPIResourceGeneration Generation {
            get { return generation; }
        }

        /// <summary>
        /// A list of the machines that teach this move
        /// </summary>
        public List<MachineVersionDetail> Machines {
            get { return machines; }
        }

        /// <summary>
        /// Metadata about this move
        /// </summary>
        public MoveMetaData Meta {
            get { return meta; }
        }

        /// <summary>
        /// The name of this move listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of move resource value changes across version groups of the game
        /// </summary>
        public List<PastMoveStatValues> PastValues {
            get { return past_values; }
        }

        /// <summary>
        /// A list of stats this moves effects and how much it effects them
        /// </summary>
        public List<MoveStatChange> StatChanges {
            get { return stat_changes; }
        }

        /// <summary>
        /// The effect the move has when used in a super contest
        /// </summary>
        public APIResourceSuperContestEffect SuperContestEffect {
            get { return super_contest_effect; }
        }

        /// <summary>
        /// The type of target that will receive the effects of the attack
        /// </summary>
        public NamedAPIResourceMoveTarget Target {
            get { return target; }
        }

        /// <summary>
        /// The elemental type of this move
        /// </summary>
        public NamedAPIResourceType Type {
            get { return type; }
        }

        #endregion
    }
}