#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveAilment {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceMove> moves;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this move ailment resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this move ailment resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of moves that cause this ailment
        /// </summary>
        public List<NamedAPIResourceMove> Moves {
            get { return moves; }
        }

        /// <summary>
        /// The name of this move ailment listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}