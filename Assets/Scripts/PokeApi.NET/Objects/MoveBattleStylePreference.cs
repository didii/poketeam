#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveBattleStylePreference {
        #region Fields

        [SerializeField] private int low_hp_preference;
        [SerializeField] private int high_hp_preference;
        [SerializeField] private NamedAPIResourceMoveBattleStyle move_battle_style;

        #endregion

        #region Properties

        /// <summary>
        /// Chance of using the move, in percent, if HP is under one half
        /// </summary>
        public int LowHpPreference {
            get { return low_hp_preference; }
        }

        /// <summary>
        /// Chance of using the move, in percent, if HP is over one half
        /// </summary>
        public int HighHpPreference {
            get { return high_hp_preference; }
        }

        /// <summary>
        /// The move battle style
        /// </summary>
        public NamedAPIResourceMoveBattleStyle MoveBattleStyle {
            get { return move_battle_style; }
        }

        #endregion
    }
}