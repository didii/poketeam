#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveCategory {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceMove> moves;
        [SerializeField] private List<Description> descriptions;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this move category resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this move category resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of moves that fall into this category
        /// </summary>
        public List<NamedAPIResourceMove> Moves {
            get { return moves; }
        }

        /// <summary>
        /// The description of this move ailment listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        #endregion
    }
}