#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveDamageClass {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Description> descriptions;
        [SerializeField] private List<NamedAPIResourceMove> moves;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this move damage class resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this move damage class resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The description of this move damage class listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        /// <summary>
        /// A list of moves that fall into this damage class
        /// </summary>
        public List<NamedAPIResourceMove> Moves {
            get { return moves; }
        }

        /// <summary>
        /// The name of this move damage class listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}