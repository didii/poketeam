#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveFlavorText {
        #region Fields

        [SerializeField] private string flavor_text;
        [SerializeField] private NamedAPIResourceLanguage language;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties

        /// <summary>
        /// The localized flavor text for an api resource in a specific language
        /// </summary>
        public string FlavorText {
            get { return flavor_text; }
        }

        /// <summary>
        /// The language this name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        /// <summary>
        /// The version group that uses this flavor text
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}