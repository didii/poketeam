#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveLearnMethod {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Description> descriptions;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<NamedAPIResourceVersionGroup> version_groups;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this move learn method resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this move learn method resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The description of this move learn method listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        /// <summary>
        /// The name of this move learn method listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of version groups where moves can be learned through this method
        /// </summary>
        public List<NamedAPIResourceVersionGroup> VersionGroups {
            get { return version_groups; }
        }

        #endregion
    }
}