﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveMetaData {
        #region Fields

        [SerializeField] private NamedAPIResourceMoveAilment ailment;
        [SerializeField] private NamedAPIResourceMove category;
        [SerializeField] private int min_hits;
        [SerializeField] private int max_hits;
        [SerializeField] private int min_turns;
        [SerializeField] private int max_turns;
        [SerializeField] private int drain;
        [SerializeField] private int healing;
        [SerializeField] private int crit_rate;
        [SerializeField] private int ailment_chance;
        [SerializeField] private int flinch_chance;
        [SerializeField] private int stat_chance;

        #endregion

        #region Properties

        /// <summary>
        /// The status ailment this move inflicts on its target
        /// </summary>
        public NamedAPIResourceMoveAilment Ailment {
            get { return ailment; }
        }

        /// <summary>
        /// The category of move this move falls under, e.g. damage or ailment
        /// </summary>
        public NamedAPIResourceMove Category {
            get { return category; }
        }

        /// <summary>
        /// The minimum number of times this move hits. Null if it always only hits once.
        /// </summary>
        public int MinHits {
            get { return min_hits; }
        }

        /// <summary>
        /// The maximum number of times this move hits. Null if it always only hits once.
        /// </summary>
        public int MaxHits {
            get { return max_hits; }
        }

        /// <summary>
        /// The minimum number of turns this move continues to take effect. Null if it always only lasts one turn.
        /// </summary>
        public int MinTurns {
            get { return min_turns; }
        }

        /// <summary>
        /// The maximum number of turns this move continues to take effect. Null if it always only lasts one turn.
        /// </summary>
        public int MaxTurns {
            get { return max_turns; }
        }

        /// <summary>
        /// HP drain (if positive) or Recoil damage (if negative), in percent of damage done
        /// </summary>
        public int Drain {
            get { return drain; }
        }

        /// <summary>
        /// The amount of hp gained by the attacking Pokemon, in percent of it's maximum HP
        /// </summary>
        public int Healing {
            get { return healing; }
        }

        /// <summary>
        /// Critical hit rate bonus
        /// </summary>
        public int CritRate {
            get { return crit_rate; }
        }

        /// <summary>
        /// The likelihood this attack will cause an ailment
        /// </summary>
        public int AilmentChance {
            get { return ailment_chance; }
        }

        /// <summary>
        /// The likelihood this attack will cause the target Pokémon to flinch
        /// </summary>
        public int FlinchChance {
            get { return flinch_chance; }
        }

        /// <summary>
        /// The likelihood this attack will cause a stat change in the target Pokémon
        /// </summary>
        public int StatChance {
            get { return stat_chance; }
        }

        #endregion
    }
}