#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveStatAffect {
        #region Fields

        [SerializeField] private int change;
        [SerializeField] private NamedAPIResourceMove move;

        #endregion

        #region Properties

        /// <summary>
        /// The maximum amount of change to the referenced stat
        /// </summary>
        public int Change {
            get { return change; }
        }

        /// <summary>
        /// The move causing the change
        /// </summary>
        public NamedAPIResourceMove Move {
            get { return move; }
        }

        #endregion
    }
}