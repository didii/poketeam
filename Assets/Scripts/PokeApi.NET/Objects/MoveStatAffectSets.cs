#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveStatAffectSets {
        #region Fields

        [SerializeField] private List<MoveStatAffect> increase;
        [SerializeField] private List<MoveStatAffect> decrease;

        #endregion

        #region Properties

        /// <summary>
        /// A list of moves and how they change the referenced stat
        /// </summary>
        public List<MoveStatAffect> Increase {
            get { return increase; }
        }

        /// <summary>
        /// A list of moves and how they change the referenced stat
        /// </summary>
        public List<MoveStatAffect> Decrease {
            get { return decrease; }
        }

        #endregion
    }
}