#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct MoveStatChange {
        #region Fields

        [SerializeField] private int change;
        [SerializeField] private NamedAPIResourceStat stat;

        #endregion

        #region Properties

        /// <summary>
        /// The amount of change
        /// </summary>
        public int Change {
            get { return change; }
        }

        /// <summary>
        /// The stat being affected
        /// </summary>
        public NamedAPIResourceStat Stat {
            get { return stat; }
        }

        #endregion
    }
}