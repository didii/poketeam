#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Name {
        #region Fields

        [SerializeField] private string name;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The localized name for an API resource in a specific language
        /// </summary>
        public string ResourceName {
            get { return name; }
        }

        /// <summary>
        /// The language this name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}