#pragma warning disable 649
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public class NamedAPIResource : APIResource {
        #region Fields

        [SerializeField] protected string name;
        #endregion

        #region Properties

        /// <summary>
        /// The name of the referenced resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        #endregion
    }

    [Serializable]
    public class NamedAPIResource<T> : APIResource<T> {
        #region Fields

        [SerializeField]
        private string name;
        #endregion

        #region Properties

        /// <summary>
        /// The name of the referenced resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        #endregion

        public static implicit operator NamedAPIResource<T>(NamedAPIResource other) {
            return new NamedAPIResource<T> {
                name = other.Name,
                url = other.Url
            };
        }

    }

    #region Workaround

    [Serializable]
    public class NamedAPIResourceBerryFirmness : NamedAPIResource<BerryFirmness> {}

    [Serializable]
    public class NamedAPIResourceItem : NamedAPIResource<Item> {}

    [Serializable]
    public class NamedAPIResourceType : NamedAPIResource<Type> {}

    [Serializable]
    public class NamedAPIResourceBerryFlavor : NamedAPIResource<BerryFlavor> {}

    [Serializable]
    public class NamedAPIResourceBerry : NamedAPIResource<Berry> {}

    [Serializable]
    public class NamedAPIResourceContestType : NamedAPIResource<ContestType> {}

    [Serializable]
    public class NamedAPIResourceLanguage : NamedAPIResource<Language> {}

    [Serializable]
    public class NamedAPIResourceMove : NamedAPIResource<Move> {}

    [Serializable]
    public class NamedAPIResourceEncounterConditionValue : NamedAPIResource<EncounterConditionValue> {}

    [Serializable]
    public class NamedAPIResourceEncounterCondition : NamedAPIResource<EncounterCondition> {}

    [Serializable]
    public class NamedAPIResourcePokemonSpecies : NamedAPIResource<PokemonSpecies> {}

    [Serializable]
    public class NamedAPIResourceEvolutionTrigger : NamedAPIResource<EvolutionTrigger> {}

    [Serializable]
    public class NamedAPIResourceLocation : NamedAPIResource<Location> {}

    [Serializable]
    public class NamedAPIResourceAbility : NamedAPIResource<Ability> {}

    [Serializable]
    public class NamedAPIResourceRegion : NamedAPIResource<Region> {}

    [Serializable]
    public class NamedAPIResourceVersionGroup : NamedAPIResource<VersionGroup> {}

    [Serializable]
    public class NamedAPIResourceGeneration : NamedAPIResource<Generation> {}

    [Serializable]
    public class NamedAPIResourceMoveLearnMethod : NamedAPIResource<MoveLearnMethod> {}

    [Serializable]
    public class NamedAPIResourcePokedex : NamedAPIResource<Pokedex> {}

    [Serializable]
    public class NamedAPIResourceVersion : NamedAPIResource<Version> {}

    [Serializable]
    public class NamedAPIResourceItemFlingEffect : NamedAPIResource<ItemFlingEffect> {}

    [Serializable]
    public class NamedAPIResourceItemAttribute : NamedAPIResource<ItemAttribute> {}

    [Serializable]
    public class NamedAPIResourceItemPocket : NamedAPIResource<ItemPocket> {}

    [Serializable]
    public class NamedAPIResourceItemCategory : NamedAPIResource<ItemCategory> {}

    [Serializable]
    public class NamedAPIResourceMoveDamageClass : NamedAPIResource<MoveDamageClass> {}

    [Serializable]
    public class NamedAPIResourceMoveTarget : NamedAPIResource<MoveTarget> {}

    [Serializable]
    public class NamedAPIResourceMoveAilment : NamedAPIResource<MoveAilment> {}

    [Serializable]
    public class NamedAPIResourceStat : NamedAPIResource<Stat> {}

    [Serializable]
    public class NamedAPIResourceLocationArea : NamedAPIResource<LocationArea> {}

    [Serializable]
    public class NamedAPIResourceEncounterMethod : NamedAPIResource<EncounterMethod> {}

    [Serializable]
    public class NamedAPIResourcePokemon : NamedAPIResource<Pokemon> {}

    [Serializable]
    public class NamedAPIResourcePokeathlonStat : NamedAPIResource<PokeathlonStat> {}

    [Serializable]
    public class NamedAPIResourceMoveBattleStyle : NamedAPIResource<MoveBattleStyle> {}

    [Serializable]
    public class NamedAPIResourceNature : NamedAPIResource<Nature> {}

    [Serializable]
    public class NamedAPIResourcePokemonForm : NamedAPIResource<PokemonForm> {}

    [Serializable]
    public class NamedAPIResourceLocationAreaEncounter : NamedAPIResource<LocationAreaEncounter> {}

    [Serializable]
    public class NamedAPIResourceGrowthRate : NamedAPIResource<GrowthRate> {}

    [Serializable]
    public class NamedAPIResourceEggGroup : NamedAPIResource<EggGroup> {}

    [Serializable]
    public class NamedAPIResourcePokemonColor : NamedAPIResource<PokemonColor> {}

    [Serializable]
    public class NamedAPIResourcePokemonShape : NamedAPIResource<PokemonShape> {}

    [Serializable]
    public class NamedAPIResourcePokemonHabitat : NamedAPIResource<PokemonHabitat> {}

    [Serializable]
    public class NamedAPIResourcePalParkArea : NamedAPIResource<PalParkArea> {}

    #endregion
}