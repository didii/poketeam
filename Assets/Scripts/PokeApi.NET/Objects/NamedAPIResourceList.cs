#pragma warning disable 649
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public class NamedAPIResourceList {
        #region Fields

        [SerializeField] private int count;
        [SerializeField] private string next;
        [SerializeField] private bool previous;
        [SerializeField] private List<NamedAPIResource> results;

        #endregion

        #region Properties

        /// <summary>
        /// The total number of resources available from this API
        /// </summary>
        public int Count {
            get { return count; }
        }

        /// <summary>
        /// The URL for the next page in the list
        /// </summary>
        public string Next {
            get { return next; }
        }

        /// <summary>
        /// The URL for the previous page in the list
        /// </summary>
        public bool Previous {
            get { return previous; }
        }

        /// <summary>
        /// A list of named API resources
        /// </summary>
        public List<NamedAPIResource> Results {
            get { return results; }
        }

        /// <summary>
        /// Get the names of the containing resources.
        /// </summary>
        public List<string> Names {
            get { return results.Select(r => r.Name).ToList(); }
        }

        /// <summary>
        /// Get the urls of the containing resources.
        /// </summary>
        public List<string> UrlList {
            get { return results.Select(r => r.Url).ToList(); }
        }
        #endregion
    }
}