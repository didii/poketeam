#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Nature {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private NamedAPIResourceStat decreased_stat;
        [SerializeField] private NamedAPIResourceStat increased_stat;
        [SerializeField] private NamedAPIResourceBerryFlavor hates_flavor;
        [SerializeField] private NamedAPIResourceBerryFlavor likes_flavor;
        [SerializeField] private List<NatureStatChange> pokeathlon_stat_changes;
        [SerializeField] private List<MoveBattleStylePreference> move_battle_style_preferences;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this nature resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this nature resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The stat decreased by 10% in Pokémon with this nature
        /// </summary>
        public NamedAPIResourceStat DecreasedStat {
            get { return decreased_stat; }
        }

        /// <summary>
        /// The stat increased by 10% in Pokémon with this nature
        /// </summary>
        public NamedAPIResourceStat IncreasedStat {
            get { return increased_stat; }
        }

        /// <summary>
        /// The flavor hated by Pokémon with this nature
        /// </summary>
        public NamedAPIResourceBerryFlavor HatesFlavor {
            get { return hates_flavor; }
        }

        /// <summary>
        /// The flavor liked by Pokémon with this nature
        /// </summary>
        public NamedAPIResourceBerryFlavor LikesFlavor {
            get { return likes_flavor; }
        }

        /// <summary>
        /// A list of Pokéathlon stats this nature effects and how much it effects them
        /// </summary>
        public List<NatureStatChange> PokeathlonStatChanges {
            get { return pokeathlon_stat_changes; }
        }

        /// <summary>
        /// A list of battle styles and how likely a Pokémon with this nature is to use them in the Battle Palace or Battle Tent.
        /// </summary>
        public List<MoveBattleStylePreference> MoveBattleStylePreferences {
            get { return move_battle_style_preferences; }
        }

        /// <summary>
        /// The name of this nature listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}