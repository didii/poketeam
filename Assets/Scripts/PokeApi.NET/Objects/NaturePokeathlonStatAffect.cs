﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct NaturePokeathlonStatAffect {
        #region Fields

        [SerializeField] private int max_change;
        [SerializeField] private NamedAPIResourceNature nature;

        #endregion

        #region Properties

        /// <summary>
        /// The maximum amount of change to the referenced Pokéathlon stat
        /// </summary>
        public int MaxChange {
            get { return max_change; }
        }

        /// <summary>
        /// The nature causing the change
        /// </summary>
        public NamedAPIResourceNature Nature {
            get { return nature; }
        }

        #endregion
    }
}