﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct NaturePokeathlonStatAffectSets {
        #region Fields

        [SerializeField] private List<NaturePokeathlonStatAffect> increase;
        [SerializeField] private List<NaturePokeathlonStatAffect> decrease;

        #endregion

        #region Properties

        /// <summary>
        /// A list of natures and how they change the referenced Pokéathlon stat
        /// </summary>
        public List<NaturePokeathlonStatAffect> Increase {
            get { return increase; }
        }

        /// <summary>
        /// A list of natures and how they change the referenced Pokéathlon stat
        /// </summary>
        public List<NaturePokeathlonStatAffect> Decrease {
            get { return decrease; }
        }

        #endregion
    }
}