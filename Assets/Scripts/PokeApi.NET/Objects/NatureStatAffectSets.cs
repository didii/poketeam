#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct NatureStatAffectSets {
        #region Fields

        [SerializeField] private List<NamedAPIResourceNature> increase;
        [SerializeField] private List<NamedAPIResourceNature> decrease;

        #endregion

        #region Properties

        /// <summary>
        /// A list of natures and how they change the referenced stat
        /// </summary>
        public List<NamedAPIResourceNature> Increase {
            get { return increase; }
        }

        /// <summary>
        /// A list of nature sand how they change the referenced stat
        /// </summary>
        public List<NamedAPIResourceNature> Decrease {
            get { return decrease; }
        }

        #endregion
    }
}