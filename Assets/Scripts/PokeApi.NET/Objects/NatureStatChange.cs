#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct NatureStatChange {
        #region Fields

        [SerializeField] private int max_change;
        [SerializeField] private NamedAPIResourcePokeathlonStat pokeathlon_stat;

        #endregion

        #region Properties

        /// <summary>
        /// The amount of change
        /// </summary>
        public int MaxChange {
            get { return max_change; }
        }

        /// <summary>
        /// The stat being affected
        /// </summary>
        public NamedAPIResourcePokeathlonStat PokeathlonStat {
            get { return pokeathlon_stat; }
        }

        #endregion
    }
}