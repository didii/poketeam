﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PalParkArea {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<PalParkEncounterSpecies> pokemon_encounters;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this pal park area resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this pal park area resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The name of this pal park area listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of Pokémon encountered in thi pal park area along with details
        /// </summary>
        public List<PalParkEncounterSpecies> PokemonEncounters {
            get { return pokemon_encounters; }
        }

        #endregion
    }
}