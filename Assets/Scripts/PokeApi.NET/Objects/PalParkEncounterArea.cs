﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PalParkEncounterArea {
        #region Fields

        [SerializeField] private int base_score;
        [SerializeField] private int rate;
        [SerializeField] private NamedAPIResourcePalParkArea area;

        #endregion

        #region Properties

        /// <summary>
        /// The base score given to the player when the referenced Pokémon is caught during a pal park run
        /// </summary>
        public int BaseScore {
            get { return base_score; }
        }

        /// <summary>
        /// The base rate for encountering the referenced Pokémon in this pal park area
        /// </summary>
        public int Rate {
            get { return rate; }
        }

        /// <summary>
        /// The pal park area where this encounter happens
        /// </summary>
        public NamedAPIResourcePalParkArea Area {
            get { return area; }
        }

        #endregion
    }
}