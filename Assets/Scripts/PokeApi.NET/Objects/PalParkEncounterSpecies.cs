﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PalParkEncounterSpecies {
        #region Fields

        [SerializeField] private int base_score;
        [SerializeField] private int rate;
        [SerializeField] private NamedAPIResourcePokemonSpecies pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The base score given to the player when this Pokémon is caught during a pal park run
        /// </summary>
        public int BaseScore {
            get { return base_score; }
        }

        /// <summary>
        /// The base rate for encountering this Pokémon in this pal park area
        /// </summary>
        public int Rate {
            get { return rate; }
        }

        /// <summary>
        /// The Pokémon species being encountered
        /// </summary>
        public NamedAPIResourcePokemonSpecies PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}