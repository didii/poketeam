#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PastMoveStatValues {
        #region Fields

        [SerializeField] private int accuracy;
        [SerializeField] private int effect_chance;
        [SerializeField] private int power;
        [SerializeField] private int pp;
        [SerializeField] private List<VerboseEffect> effect_entries;
        [SerializeField] private NamedAPIResourceType type;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties

        /// <summary>
        /// The percent value of how likely this move is to be successful
        /// </summary>
        public int Accuracy {
            get { return accuracy; }
        }

        /// <summary>
        /// The percent value of how likely it is this moves effect will take effect
        /// </summary>
        public int EffectChance {
            get { return effect_chance; }
        }

        /// <summary>
        /// The base power of this move with a value of 0 if it does not have a base power
        /// </summary>
        public int Power {
            get { return power; }
        }

        /// <summary>
        /// Power points. The number of times this move can be used
        /// </summary>
        public int Pp {
            get { return pp; }
        }

        /// <summary>
        /// The effect of this move listed in different languages
        /// </summary>
        public List<VerboseEffect> EffectEntries {
            get { return effect_entries; }
        }

        /// <summary>
        /// The elemental type of this move
        /// </summary>
        public NamedAPIResourceType Type {
            get { return type; }
        }

        /// <summary>
        /// The version group in which these move stat values were in effect
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}