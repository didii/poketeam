﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokeathlonStat {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Name> names;
        [SerializeField] private NaturePokeathlonStatAffectSets affecting_natures;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokéathlon stat resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokéathlon stat resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The name of this Pokéathlon stat listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A detail of natures which affect this Pokéathlon stat positively or negatively
        /// </summary>
        public NaturePokeathlonStatAffectSets AffectingNatures {
            get { return affecting_natures; }
        }

        #endregion
    }
}