﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Pokedex {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private bool is_main_series;
        [SerializeField] private List<Description> descriptions;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<PokemonEntry> pokemon_entries;
        [SerializeField] private NamedAPIResourceRegion region;
        [SerializeField] private List<NamedAPIResourceVersionGroup> version_groups;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokédex resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokédex resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// Whether or not this Pokédex originated in the main series of the video games
        /// </summary>
        public bool IsMainSeries {
            get { return is_main_series; }
        }

        /// <summary>
        /// The description of this Pokédex listed in different languages
        /// </summary>
        public List<Description> Descriptions {
            get { return descriptions; }
        }

        /// <summary>
        /// The name of this Pokédex listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of Pokémon catalogued in this Pokédex and their indexes
        /// </summary>
        public List<PokemonEntry> PokemonEntries {
            get { return pokemon_entries; }
        }

        /// <summary>
        /// The region this Pokédex catalogues Pokémon for
        /// </summary>
        public NamedAPIResourceRegion Region {
            get { return region; }
        }

        /// <summary>
        /// A list of version groups this Pokédex is relevant to
        /// </summary>
        public List<NamedAPIResourceVersionGroup> VersionGroups {
            get { return version_groups; }
        }

        #endregion
    }
}