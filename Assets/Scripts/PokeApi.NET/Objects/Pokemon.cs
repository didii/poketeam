﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Pokemon {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int base_experience;
        [SerializeField] private int height;
        [SerializeField] private bool is_default;
        [SerializeField] private int order;
        [SerializeField] private int weight;
        [SerializeField] private List<PokemonAbility> abilities;
        [SerializeField] private List<NamedAPIResourcePokemonForm> forms;
        [SerializeField] private List<VersionGameIndex> game_indices;
        [SerializeField] private List<PokemonHeldItem> held_items;
        [SerializeField] private NamedAPIResourceLocationAreaEncounter location_area_encounters;
        [SerializeField] private List<PokemonMove> moves;
        [SerializeField] private PokemonSprites sprites;
        [SerializeField] private NamedAPIResourcePokemonSpecies species;
        [SerializeField] private List<PokemonStat> stats;
        [SerializeField] private List<PokemonType> types;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokémon resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokémon resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The base experience gained for defeating this Pokémon
        /// </summary>
        public int BaseExperience {
            get { return base_experience; }
        }

        /// <summary>
        /// The height of this Pokémon
        /// </summary>
        public int Height {
            get { return height; }
        }

        /// <summary>
        /// Set for exactly one Pokémon used as the default for each species
        /// </summary>
        public bool IsDefault {
            get { return is_default; }
        }

        /// <summary>
        /// Order for sorting. Almost national order, except families are grouped together.
        /// </summary>
        public int Order {
            get { return order; }
        }

        /// <summary>
        /// The weight of this Pokémon
        /// </summary>
        public int Weight {
            get { return weight; }
        }

        /// <summary>
        /// A list of abilities this Pokémon could potentially have
        /// </summary>
        public List<PokemonAbility> Abilities {
            get { return abilities; }
        }

        /// <summary>
        /// A list of forms this Pokémon can take on
        /// </summary>
        public List<NamedAPIResourcePokemonForm> Forms {
            get { return forms; }
        }

        /// <summary>
        /// A list of game indices relevent to Pokémon item by generation
        /// </summary>
        public List<VersionGameIndex> GameIndices {
            get { return game_indices; }
        }

        /// <summary>
        /// A list of items this Pokémon may be holding when encountered
        /// </summary>
        public List<PokemonHeldItem> HeldItems {
            get { return held_items; }
        }

        /// <summary>
        /// A link to a list of location areas as well as encounter details pertaining to specific versions
        /// </summary>
        public NamedAPIResourceLocationAreaEncounter LocationAreaEncounters {
            get { return location_area_encounters; }
        }

        /// <summary>
        /// A list of moves along with learn methods and level details pertaining to specific version groups
        /// </summary>
        public List<PokemonMove> Moves {
            get { return moves; }
        }

        /// <summary>
        /// A set of sprites used to depict this Pokémon in the game
        /// </summary>
        public PokemonSprites Sprites {
            get { return sprites; }
        }

        /// <summary>
        /// The species this Pokémon belongs to
        /// </summary>
        public NamedAPIResourcePokemonSpecies Species {
            get { return species; }
        }

        /// <summary>
        /// A list of base stat values for this Pokémon
        /// </summary>
        public List<PokemonStat> Stats {
            get { return stats; }
        }

        /// <summary>
        /// A list of details showing types this Pokémon has
        /// </summary>
        public List<PokemonType> Types {
            get { return types; }
        }

        #endregion
    }
}