﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonAbility {
        #region Fields

        [SerializeField] private bool is_hidden;
        [SerializeField] private int slot;
        [SerializeField] private NamedAPIResourceAbility ability;

        #endregion

        #region Properties

        /// <summary>
        /// Whether or not this is a hidden ability
        /// </summary>
        public bool IsHidden {
            get { return is_hidden; }
        }

        /// <summary>
        /// The slot this ability occupies in this Pokémon species
        /// </summary>
        public int Slot {
            get { return slot; }
        }

        /// <summary>
        /// The ability the Pokémon may have
        /// </summary>
        public NamedAPIResourceAbility Ability {
            get { return ability; }
        }

        #endregion
    }
}