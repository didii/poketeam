﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonEncounter {
        #region Fields

        [SerializeField] private NamedAPIResourcePokemon pokemon;
        [SerializeField] private List<VersionEncounterDetail> version_details;

        #endregion

        #region Properties

        /// <summary>
        /// The Pokémon being encountered
        /// </summary>
        public NamedAPIResourcePokemon Pokemon {
            get { return pokemon; }
        }

        /// <summary>
        /// A list of versions and encounters with Pokémon that might happen in the referenced location area
        /// </summary>
        public List<VersionEncounterDetail> VersionDetails {
            get { return version_details; }
        }

        #endregion
    }
}