﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonEntry {
        #region Fields

        [SerializeField] private int entry_number;
        [SerializeField] private NamedAPIResourcePokemonSpecies pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The index of this Pokémon species entry within the Pokédex
        /// </summary>
        public int EntryNumber {
            get { return entry_number; }
        }

        /// <summary>
        /// The Pokémon species being encountered
        /// </summary>
        public NamedAPIResourcePokemonSpecies PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}