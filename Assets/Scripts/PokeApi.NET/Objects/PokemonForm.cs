﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonForm {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int order;
        [SerializeField] private int form_order;
        [SerializeField] private bool is_default;
        [SerializeField] private bool is_battle_only;
        [SerializeField] private bool is_mega;
        [SerializeField] private string form_name;
        [SerializeField] private NamedAPIResourcePokemon pokemon;
        [SerializeField] private PokemonFormSprites sprites;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<Name> form_names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokémon form resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokémon form resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The order in which forms should be sorted within all forms. Multiple forms may have equal order, in which case they should fall back on sorting by name.
        /// </summary>
        public int Order {
            get { return order; }
        }

        /// <summary>
        /// The order in which forms should be sorted within a species' forms
        /// </summary>
        public int FormOrder {
            get { return form_order; }
        }

        /// <summary>
        /// True for exactly one form used as the default for each Pokémon
        /// </summary>
        public bool IsDefault {
            get { return is_default; }
        }

        /// <summary>
        /// Whether or not this form can only happen during battle
        /// </summary>
        public bool IsBattleOnly {
            get { return is_battle_only; }
        }

        /// <summary>
        /// Whether or not this form requires mega evolution
        /// </summary>
        public bool IsMega {
            get { return is_mega; }
        }

        /// <summary>
        /// The name of this form
        /// </summary>
        public string FormName {
            get { return form_name; }
        }

        /// <summary>
        /// The Pokémon that can take on this form
        /// </summary>
        public NamedAPIResourcePokemon Pokemon {
            get { return pokemon; }
        }

        /// <summary>
        /// A set of sprites used to depict this Pokémon form in the game
        /// </summary>
        public PokemonFormSprites Sprites {
            get { return sprites; }
        }

        /// <summary>
        /// The version group this Pokémon form was introduced in
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        /// <summary>
        /// The form specific full name of this Pokémon form, or empty if the form does not have a specific name
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// The form specific form name of this Pokémon form, or empty if the form does not have a specific name
        /// </summary>
        public List<Name> FormNames {
            get { return form_names; }
        }

        #endregion
    }
}