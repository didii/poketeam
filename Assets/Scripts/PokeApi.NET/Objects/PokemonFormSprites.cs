﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonFormSprites {
        #region Fields

        [SerializeField] private string front_default;
        [SerializeField] private string front_shiny;
        [SerializeField] private string back_default;
        [SerializeField] private string back_shiny;

        #endregion

        #region Properties

        /// <summary>
        /// The default depiction of this Pokémon form from the front in battle
        /// </summary>
        public string FrontDefault {
            get { return front_default; }
        }

        /// <summary>
        /// The shiny depiction of this Pokémon form from the front in battle
        /// </summary>
        public string FrontShiny {
            get { return front_shiny; }
        }

        /// <summary>
        /// The default depiction of this Pokémon form from the back in battle
        /// </summary>
        public string BackDefault {
            get { return back_default; }
        }

        /// <summary>
        /// The shiny depiction of this Pokémon form from the back in battle
        /// </summary>
        public string BackShiny {
            get { return back_shiny; }
        }

        #endregion
    }
}