﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonHabitat {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<NamedAPIResourcePokemonSpecies> pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokémon habitat resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokémon habitat resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The name of this Pokémon habitat listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of the Pokémon species that can be found in this habitat
        /// </summary>
        public List<NamedAPIResourcePokemonSpecies> PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}