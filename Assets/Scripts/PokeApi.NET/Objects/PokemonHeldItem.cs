﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonHeldItem {
        #region Fields

        [SerializeField] private NamedAPIResourceItem item;
        [SerializeField] private List<PokemonHeldItemVersion> version_details;

        #endregion

        #region Properties

        /// <summary>
        /// The item the referenced Pokémon holds
        /// </summary>
        public NamedAPIResourceItem Item {
            get { return item; }
        }

        /// <summary>
        /// The details of the different versions in which the item is held
        /// </summary>
        public List<PokemonHeldItemVersion> VersionDetails {
            get { return version_details; }
        }

        #endregion
    }
}