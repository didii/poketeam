#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonHeldItemVersion {
        #region Fields

        [SerializeField] private NamedAPIResourceVersion version;
        [SerializeField] private int rarity;

        #endregion

        #region Properties

        /// <summary>
        /// The version in which the item is held
        /// </summary>
        public NamedAPIResourceVersion Version {
            get { return version; }
        }

        /// <summary>
        /// How often the item is held
        /// </summary>
        public int Rarity {
            get { return rarity; }
        }

        #endregion
    }
}