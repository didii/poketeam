﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonMove {
        #region Fields

        [SerializeField] private NamedAPIResourceMove move;
        [SerializeField] private List<PokemonMoveVersion> version_group_details;

        #endregion

        #region Properties

        /// <summary>
        /// The move the Pokémon can learn
        /// </summary>
        public NamedAPIResourceMove Move {
            get { return move; }
        }

        /// <summary>
        /// The details of the version in which the Pokémon can learn the move
        /// </summary>
        public List<PokemonMoveVersion> VersionGroupDetails {
            get { return version_group_details; }
        }

        #endregion
    }
}