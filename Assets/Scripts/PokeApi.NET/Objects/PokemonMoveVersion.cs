#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonMoveVersion {
        #region Fields

        [SerializeField] private NamedAPIResourceMoveLearnMethod move_learn_method;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;
        [SerializeField] private int level_learned_at;

        #endregion

        #region Properties

        /// <summary>
        /// The method by which the move is learned
        /// </summary>
        public NamedAPIResourceMoveLearnMethod MoveLearnMethod {
            get { return move_learn_method; }
        }

        /// <summary>
        /// The version group in which the move is learned
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        /// <summary>
        /// The minimum level to learn the move
        /// </summary>
        public int LevelLearnedAt {
            get { return level_learned_at; }
        }

        #endregion
    }
}