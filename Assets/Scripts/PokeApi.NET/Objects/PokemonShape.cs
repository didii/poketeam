﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonShape {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<AwesomeName> awesome_names;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<NamedAPIResourcePokemonSpecies> pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokémon shape resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokémon shape resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The "scientific" name of this Pokémon shape listed in different languages
        /// </summary>
        public List<AwesomeName> AwesomeNames {
            get { return awesome_names; }
        }

        /// <summary>
        /// The name of this Pokémon shape listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of the Pokémon species that have this shape
        /// </summary>
        public List<NamedAPIResourcePokemonSpecies> PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}