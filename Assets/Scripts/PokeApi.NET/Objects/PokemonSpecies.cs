#pragma warning disable 649
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonSpecies {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int order;
        [SerializeField] private int gender_rate;
        [SerializeField] private int capture_rate;
        [SerializeField] private int base_happiness;
        [SerializeField] private bool is_baby;
        [SerializeField] private int hatch_counter;
        [SerializeField] private bool has_gender_differences;
        [SerializeField] private bool forms_switchable;
        [SerializeField] private NamedAPIResourceGrowthRate growth_rate;
        [SerializeField] private List<PokemonSpeciesDexEntry> pokedex_numbers;
        [SerializeField] private List<NamedAPIResourceEggGroup> egg_groups;
        [SerializeField] private NamedAPIResourcePokemonColor color;
        [SerializeField] private NamedAPIResourcePokemonShape shape;
        [SerializeField] private NamedAPIResourcePokemonSpecies evolves_from_species;
        [SerializeField] private APIResourceEvolutionChain evolution_chain;
        [SerializeField] private NamedAPIResourcePokemonHabitat habitat;
        [SerializeField] private NamedAPIResourceGeneration generation;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<PalParkEncounterArea> pal_park_encounters;
        [SerializeField] private List<FlavorText> flavor_text_entries;
        [SerializeField] private List<Description> form_descriptions;
        [SerializeField] private List<Genus> genera;
        [SerializeField] private List<PokemonSpeciesVariety> varieties;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this Pokémon species resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this Pokémon species resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The order in which species should be sorted. Based on National Dex order, except families are grouped together and sorted by stage.
        /// </summary>
        public int Order {
            get { return order; }
        }

        /// <summary>
        /// The chance of this Pokémon being female, in eighths; or -1 for genderless
        /// </summary>
        public int GenderRate {
            get { return gender_rate; }
        }

        /// <summary>
        /// The chance of this Pokémon being female; or null for genderless
        /// </summary>
        public float? GenderRateFemaleToMale {
            get { return gender_rate != -1 ? gender_rate/8f : (float?)null; }
        }

        /// <summary>
        /// The base capture rate; up to 255. The higher the number, the easier the catch.
        /// </summary>
        public int CaptureRate {
            get { return capture_rate; }
        }

        /// <summary>
        /// The happiness when caught by a normal Pokéball; up to 255. The higher the number, the happier the Pokémon.
        /// </summary>
        public int BaseHappiness {
            get { return base_happiness; }
        }

        /// <summary>
        /// Whether or not this is a baby Pokémon
        /// </summary>
        public bool IsBaby {
            get { return is_baby; }
        }

        /// <summary>
        /// Initial hatch counter: one must walk 255 × (hatch_counter + 1) steps before this Pokémon's egg hatches, unless utilizing bonuses like Flame Body's
        /// </summary>
        public int HatchCounter {
            get { return hatch_counter; }
        }

        /// <summary>
        /// Whether or not this Pokémon has visual gender differences
        /// </summary>
        public bool HasGenderDifferences {
            get { return has_gender_differences; }
        }

        /// <summary>
        /// Whether or not this Pokémon has multiple forms and can switch between them
        /// </summary>
        public bool FormsSwitchable {
            get { return forms_switchable; }
        }

        /// <summary>
        /// The rate at which this Pokémon species gains levels
        /// </summary>
        public NamedAPIResourceGrowthRate GrowthRate {
            get { return growth_rate; }
        }

        /// <summary>
        /// A list of Pokedexes and the indexes reserved within them for this Pokémon species
        /// </summary>
        public List<PokemonSpeciesDexEntry> PokedexNumbers {
            get { return pokedex_numbers; }
        }

        /// <summary>
        /// A list of egg groups this Pokémon species is a member of
        /// </summary>
        public List<NamedAPIResourceEggGroup> EggGroups {
            get { return egg_groups; }
        }

        /// <summary>
        /// The color of this Pokémon for gimmicky Pokédex search
        /// </summary>
        public NamedAPIResourcePokemonColor Color {
            get { return color; }
        }

        /// <summary>
        /// The shape of this Pokémon for gimmicky Pokédex search
        /// </summary>
        public NamedAPIResourcePokemonShape Shape {
            get { return shape; }
        }

        /// <summary>
        /// The Pokémon species that evolves into this Pokemon_species
        /// </summary>
        public NamedAPIResourcePokemonSpecies EvolvesFromSpecies {
            get { return evolves_from_species; }
        }

        /// <summary>
        /// The evolution chain this Pokémon species is a member of
        /// </summary>
        public APIResourceEvolutionChain EvolutionChain {
            get { return evolution_chain; }
        }

        /// <summary>
        /// The habitat this Pokémon species can be encountered in
        /// </summary>
        public NamedAPIResourcePokemonHabitat Habitat {
            get { return habitat; }
        }

        /// <summary>
        /// The generation this Pokémon species was introduced in
        /// </summary>
        public NamedAPIResourceGeneration Generation {
            get { return generation; }
        }

        /// <summary>
        /// The name of this Pokémon species listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of encounters that can be had with this Pokémon species in pal park
        /// </summary>
        public List<PalParkEncounterArea> PalParkEncounters {
            get { return pal_park_encounters; }
        }

        /// <summary>
        /// A list of flavor text entries for this Pokémon species
        /// </summary>
        public List<FlavorText> FlavorTextEntries {
            get { return flavor_text_entries; }
        }

        /// <summary>
        /// Descriptions of different forms Pokémon take on within the Pokémon species
        /// </summary>
        public List<Description> FormDescriptions {
            get { return form_descriptions; }
        }

        /// <summary>
        /// The genus of this Pokémon species listed in multiple languages
        /// </summary>
        public List<Genus> Genera {
            get { return genera; }
        }

        /// <summary>
        /// A list of the Pokémon that exist within this Pokémon species
        /// </summary>
        public List<PokemonSpeciesVariety> Varieties {
            get { return varieties; }
        }

        #endregion
    }
}