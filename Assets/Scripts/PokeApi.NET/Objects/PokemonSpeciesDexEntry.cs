﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonSpeciesDexEntry {
        #region Fields

        [SerializeField] private int entry_number;
        [SerializeField] private NamedAPIResourcePokedex pokedex;

        #endregion

        #region Properties

        /// <summary>
        /// The index number within the Pokédex
        /// </summary>
        public int EntryNumber {
            get { return entry_number; }
        }

        /// <summary>
        /// The Pokédex the referenced Pokémon species can be found in
        /// </summary>
        public NamedAPIResourcePokedex Pokedex {
            get { return pokedex; }
        }

        #endregion
    }
}