﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonSpeciesGender {
        #region Fields

        [SerializeField] private int rate;
        [SerializeField] private NamedAPIResourcePokemonSpecies pokemon_species;

        #endregion

        #region Properties

        /// <summary>
        /// The chance of this Pokémon being female, in eighths; or -1 for genderless
        /// </summary>
        public int Rate {
            get { return rate; }
        }

        /// <summary>
        /// A Pokémon species that can be the referenced gender
        /// </summary>
        public NamedAPIResourcePokemonSpecies PokemonSpecies {
            get { return pokemon_species; }
        }

        #endregion
    }
}