﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonSpeciesVariety {
        #region Fields

        [SerializeField] private bool is_default;
        [SerializeField] private NamedAPIResourcePokemon pokemon;

        #endregion

        #region Properties

        /// <summary>
        /// Whether this variety is the default variety
        /// </summary>
        public bool IsDefault {
            get { return is_default; }
        }

        /// <summary>
        /// The Pokémon variety
        /// </summary>
        public NamedAPIResourcePokemon Pokemon {
            get { return pokemon; }
        }

        #endregion
    }
}