﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonSprites {
        #region Fields

        [SerializeField] private string front_default;
        [SerializeField] private string front_shiny;
        [SerializeField] private string front_female;
        [SerializeField] private string front_shiny_female;
        [SerializeField] private string back_default;
        [SerializeField] private string back_shiny;
        [SerializeField] private string back_female;
        [SerializeField] private string back_shiny_female;

        #endregion

        #region Properties

        /// <summary>
        /// The default depiction of this Pokémon from the front in battle
        /// </summary>
        public string FrontDefault {
            get { return front_default; }
        }

        /// <summary>
        /// The shiny depiction of this Pokémon from the front in battle
        /// </summary>
        public string FrontShiny {
            get { return front_shiny; }
        }

        /// <summary>
        /// The female depiction of this Pokémon from the front in battle
        /// </summary>
        public string FrontFemale {
            get { return front_female; }
        }

        /// <summary>
        /// The shiny female depiction of this Pokémon from the front in battle
        /// </summary>
        public string FrontShinyFemale {
            get { return front_shiny_female; }
        }

        /// <summary>
        /// The default depiction of this Pokémon from the back in battle
        /// </summary>
        public string BackDefault {
            get { return back_default; }
        }

        /// <summary>
        /// The shiny depiction of this Pokémon from the back in battle
        /// </summary>
        public string BackShiny {
            get { return back_shiny; }
        }

        /// <summary>
        /// The female depiction of this Pokémon from the back in battle
        /// </summary>
        public string BackFemale {
            get { return back_female; }
        }

        /// <summary>
        /// The shiny female depiction of this Pokémon from the back in battle
        /// </summary>
        public string BackShinyFemale {
            get { return back_shiny_female; }
        }

        #endregion
    }
}