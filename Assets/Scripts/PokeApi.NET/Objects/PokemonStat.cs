﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonStat {
        #region Fields

        [SerializeField] private NamedAPIResourceStat stat;
        [SerializeField] private int effort;
        [SerializeField] private int base_stat;

        #endregion

        #region Properties

        /// <summary>
        /// The stat the Pokémon has
        /// </summary>
        public NamedAPIResourceStat Stat {
            get { return stat; }
        }

        /// <summary>
        /// The effort points (EV) the Pokémon has in the stat
        /// </summary>
        public int Effort {
            get { return effort; }
        }

        /// <summary>
        /// The base value of the stst
        /// </summary>
        public int BaseStat {
            get { return base_stat; }
        }

        #endregion
    }
}