﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct PokemonType {
        #region Fields

        [SerializeField] private int slot;
        [SerializeField] private NamedAPIResourceType type;

        #endregion

        #region Properties

        /// <summary>
        /// The order the Pokémon's types are listed in
        /// </summary>
        public int Slot {
            get { return slot; }
        }

        /// <summary>
        /// The type the referenced Pokémon has
        /// </summary>
        public NamedAPIResourceType Type {
            get { return type; }
        }

        #endregion
    }
}