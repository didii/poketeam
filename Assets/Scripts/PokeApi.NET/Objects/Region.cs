﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Region {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<NamedAPIResourceLocation> locations;
        [SerializeField] private NamedAPIResourceGeneration main_generation;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<NamedAPIResourcePokedex> pokedexes;
        [SerializeField] private List<NamedAPIResourceVersionGroup> version_groups;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this region resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this region resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A list of locations that can be found in this region
        /// </summary>
        public List<NamedAPIResourceLocation> Locations {
            get { return locations; }
        }

        /// <summary>
        /// The generation this region was introduced in
        /// </summary>
        public NamedAPIResourceGeneration MainGeneration {
            get { return main_generation; }
        }

        /// <summary>
        /// The name of this region listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of pokédexes that catalogue Pokémon in this region
        /// </summary>
        public List<NamedAPIResourcePokedex> Pokedexes {
            get { return pokedexes; }
        }

        /// <summary>
        /// A list of version groups where this region can be visited
        /// </summary>
        public List<NamedAPIResourceVersionGroup> VersionGroups {
            get { return version_groups; }
        }

        #endregion
    }
}