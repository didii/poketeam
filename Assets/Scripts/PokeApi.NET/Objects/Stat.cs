﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Stat {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int game_index;
        [SerializeField] private bool is_battle_only;
        [SerializeField] private MoveStatAffectSets affecting_moves;
        [SerializeField] private NatureStatAffectSets affecting_natures;
        [SerializeField] private List<APIResourceCharacteristic> characteristics;
        [SerializeField] private NamedAPIResourceMoveDamageClass move_damage_class;
        [SerializeField] private List<Name> names;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this stat resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this stat resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// ID the games use for this stat
        /// </summary>
        public int GameIndex {
            get { return game_index; }
        }

        /// <summary>
        /// Whether this stat only exists within a battle
        /// </summary>
        public bool IsBattleOnly {
            get { return is_battle_only; }
        }

        /// <summary>
        /// A detail of moves which affect this stat positively or negatively
        /// </summary>
        public MoveStatAffectSets AffectingMoves {
            get { return affecting_moves; }
        }

        /// <summary>
        /// A detail of natures which affect this stat positively or negatively
        /// </summary>
        public NatureStatAffectSets AffectingNatures {
            get { return affecting_natures; }
        }

        /// <summary>
        /// A list of characteristics that are set on a Pokémon when its highest base stat is this stat
        /// </summary>
        public List<APIResourceCharacteristic> Characteristics {
            get { return characteristics; }
        }

        /// <summary>
        /// The class of damage this stat is directly related to
        /// </summary>
        public NamedAPIResourceMoveDamageClass MoveDamageClass {
            get { return move_damage_class; }
        }

        /// <summary>
        /// The name of this region listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        #endregion
    }
}