#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct SuperContestEffect {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private int appeal;
        [SerializeField] private List<FlavorText> flavor_text_entries;
        [SerializeField] private List<NamedAPIResourceMove> moves;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this super contest effect resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The level of appeal this super contest effect has
        /// </summary>
        public int Appeal {
            get { return appeal; }
        }

        /// <summary>
        /// The flavor text of this super contest effect listed in different languages
        /// </summary>
        public List<FlavorText> FlavorTextEntries {
            get { return flavor_text_entries; }
        }

        /// <summary>
        /// A list of moves that have the effect when used in super contests
        /// </summary>
        public List<NamedAPIResourceMove> Moves {
            get { return moves; }
        }

        #endregion
    }
}