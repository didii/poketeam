﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Type {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private TypeRelations damage_relations;
        [SerializeField] private List<GenerationGameIndex> game_indices;
        [SerializeField] private NamedAPIResourceGeneration generation;
        [SerializeField] private NamedAPIResourceMoveDamageClass move_damage_class;
        [SerializeField] private List<Name> names;
        [SerializeField] private List<TypePokemon> pokemon;
        [SerializeField] private List<NamedAPIResourceMove> moves;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this type resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this type resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// A detail of how effective this type is toward others and vice versa
        /// </summary>
        public TypeRelations DamageRelations {
            get { return damage_relations; }
        }

        /// <summary>
        /// A list of game indices relevent to this item by generation
        /// </summary>
        public List<GenerationGameIndex> GameIndices {
            get { return game_indices; }
        }

        /// <summary>
        /// The generation this type was introduced in
        /// </summary>
        public NamedAPIResourceGeneration Generation {
            get { return generation; }
        }

        /// <summary>
        /// The class of damage inflicted by this type
        /// </summary>
        public NamedAPIResourceMoveDamageClass MoveDamageClass {
            get { return move_damage_class; }
        }

        /// <summary>
        /// The name of this type listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// A list of details of Pokémon that have this type
        /// </summary>
        public List<TypePokemon> Pokemon {
            get { return pokemon; }
        }

        /// <summary>
        /// A list of moves that have this type
        /// </summary>
        public List<NamedAPIResourceMove> Moves {
            get { return moves; }
        }

        #endregion
    }
}