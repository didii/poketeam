﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct TypePokemon {
        #region Fields

        [SerializeField] private int slot;
        [SerializeField] private NamedAPIResourcePokemon pokemon;

        #endregion

        #region Properties

        /// <summary>
        /// The order the Pokémon's types are listed in
        /// </summary>
        public int Slot {
            get { return slot; }
        }

        /// <summary>
        /// The Pokémon that has the referenced type
        /// </summary>
        public NamedAPIResourcePokemon Pokemon {
            get { return pokemon; }
        }

        #endregion
    }
}