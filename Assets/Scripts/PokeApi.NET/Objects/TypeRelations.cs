#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct TypeRelations {
        #region Fields

        [SerializeField] private List<NamedAPIResourceType> no_damage_to;
        [SerializeField] private List<NamedAPIResourceType> half_damage_to;
        [SerializeField] private List<NamedAPIResourceType> double_damage_to;
        [SerializeField] private List<NamedAPIResourceType> no_damage_from;
        [SerializeField] private List<NamedAPIResourceType> half_damage_from;
        [SerializeField] private List<NamedAPIResourceType> double_damage_from;

        #endregion

        #region Properties

        /// <summary>
        /// A list of types this type has no effect on
        /// </summary>
        public List<NamedAPIResourceType> NoDamageTo {
            get { return no_damage_to; }
        }

        /// <summary>
        /// A list of types this type is not very effect against
        /// </summary>
        public List<NamedAPIResourceType> HalfDamageTo {
            get { return half_damage_to; }
        }

        /// <summary>
        /// A list of types this type is very effect against
        /// </summary>
        public List<NamedAPIResourceType> DoubleDamageTo {
            get { return double_damage_to; }
        }

        /// <summary>
        /// A list of types that have no effect on this type
        /// </summary>
        public List<NamedAPIResourceType> NoDamageFrom {
            get { return no_damage_from; }
        }

        /// <summary>
        /// A list of types that are not very effective against this type
        /// </summary>
        public List<NamedAPIResourceType> HalfDamageFrom {
            get { return half_damage_from; }
        }

        /// <summary>
        /// A list of types that are very effective against this type
        /// </summary>
        public List<NamedAPIResourceType> DoubleDamageFrom {
            get { return double_damage_from; }
        }

        #endregion
    }
}