#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct VerboseEffect {
        #region Fields

        [SerializeField] private string effect;
        [SerializeField] private string short_effect;
        [SerializeField] private NamedAPIResourceLanguage language;

        #endregion

        #region Properties

        /// <summary>
        /// The localized effect text for an API resource in a specific language
        /// </summary>
        public string Effect {
            get { return effect; }
        }

        /// <summary>
        /// The localized effect text in brief
        /// </summary>
        public string ShortEffect {
            get { return short_effect; }
        }

        /// <summary>
        /// The language this effect is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        #endregion
    }
}