#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct Version {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private List<Name> names;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this version resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this version resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// The name of this version listed in different languages
        /// </summary>
        public List<Name> Names {
            get { return names; }
        }

        /// <summary>
        /// The version group this version belongs to
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}