#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct VersionEncounterDetail {
        #region Fields

        [SerializeField] private NamedAPIResourceVersion version;
        [SerializeField] private int max_chance;
        [SerializeField] private List<Encounter> encounter_details;

        #endregion

        #region Properties

        /// <summary>
        /// The game version this encounter happens in
        /// </summary>
        public NamedAPIResourceVersion Version {
            get { return version; }
        }

        /// <summary>
        /// The total percentage of all encounter potential
        /// </summary>
        public int MaxChance {
            get { return max_chance; }
        }

        /// <summary>
        /// A list of encounters and their specifics
        /// </summary>
        public List<Encounter> EncounterDetails {
            get { return encounter_details; }
        }

        #endregion
    }
}