#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct VersionGameIndex {
        #region Fields

        [SerializeField] private int game_index;
        [SerializeField] private NamedAPIResourceVersion version;

        #endregion

        #region Properties

        /// <summary>
        /// The internal id of an API resource within game data
        /// </summary>
        public int GameIndex {
            get { return game_index; }
        }

        /// <summary>
        /// The version relevent to this game index
        /// </summary>
        public NamedAPIResourceVersion Version {
            get { return version; }
        }

        #endregion
    }
}