﻿#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct VersionGroup {
        #region Fields

        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private int order;
        [SerializeField] private NamedAPIResourceGeneration generation;
        [SerializeField] private List<NamedAPIResourceMoveLearnMethod> move_learn_methods;
        [SerializeField] private List<NamedAPIResourcePokedex> pokedexes;
        [SerializeField] private List<NamedAPIResourceRegion> regions;
        [SerializeField] private List<NamedAPIResourceVersion> versions;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier for this version group resource
        /// </summary>
        public int Id {
            get { return id; }
        }

        /// <summary>
        /// The name for this version group resource
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// Order for sorting. Almost by date of release, except similar versions are grouped together.
        /// </summary>
        public int Order {
            get { return order; }
        }

        /// <summary>
        /// The generation this version was introduced in
        /// </summary>
        public NamedAPIResourceGeneration Generation {
            get { return generation; }
        }

        /// <summary>
        /// A list of methods in which Pokémon can learn moves in this version group
        /// </summary>
        public List<NamedAPIResourceMoveLearnMethod> MoveLearnMethods {
            get { return move_learn_methods; }
        }

        /// <summary>
        /// A list of Pokédexes introduces in this version group
        /// </summary>
        public List<NamedAPIResourcePokedex> Pokedexes {
            get { return pokedexes; }
        }

        /// <summary>
        /// A list of regions that can be visited in this version group
        /// </summary>
        public List<NamedAPIResourceRegion> Regions {
            get { return regions; }
        }

        /// <summary>
        /// The versions this version group owns
        /// </summary>
        public List<NamedAPIResourceVersion> Versions {
            get { return versions; }
        }

        #endregion
    }
}