#pragma warning disable 649
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PokeApi {
    [Serializable]
    public struct VersionGroupFlavorText {
        #region Fields

        [SerializeField] private string text;
        [SerializeField] private NamedAPIResourceLanguage language;
        [SerializeField] private NamedAPIResourceVersionGroup version_group;

        #endregion

        #region Properties

        /// <summary>
        /// The localized name for an API resource in a specific language
        /// </summary>
        public string Text {
            get { return text; }
        }

        /// <summary>
        /// The language this name is in
        /// </summary>
        public NamedAPIResourceLanguage Language {
            get { return language; }
        }

        /// <summary>
        /// The version group which uses this flavor text
        /// </summary>
        public NamedAPIResourceVersionGroup VersionGroup {
            get { return version_group; }
        }

        #endregion
    }
}