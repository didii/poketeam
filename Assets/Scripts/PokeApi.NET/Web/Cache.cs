﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace PokeApi.Web {
    public class Cache {
        #region Fields

        protected string _cacheDir = "";
        protected string _cacheFile = "PokeApi.cache";
        protected Dictionary<string, string> _cache = new Dictionary<string, string>();

        private readonly DataFetcher _fetcher = new DataFetcher();
        protected string _lastJson;
        #endregion

        #region Properties
        /// <summary>
        /// Directory of cache file.
        /// </summary>
        /// <seealso cref="Save"/><seealso cref="Load"/>
        public string CacheDir {
            get { return _cacheDir; }
            set { _cacheDir = value; }
        }

        /// <summary>
        /// Filename of cache.
        /// </summary>
        /// <seealso cref="Save"/><seealso cref="Load"/>
        public string CacheFile {
            get { return _cacheFile; }
            set { _cacheFile = value; }
        }

        /// <summary>
        /// The last retreived json
        /// </summary>
        public string LastJson {
            get { return _lastJson; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor, initializes all variables and tries loading cache on disk from default location.
        /// </summary>
        public Cache(bool initLoad =true) {
            if (initLoad)
                Load();
        }

        /// <summary>
        /// Constructor with cache file specified. Immediatly loads it when present.
        /// </summary>
        /// <param name="file">Cache save file</param>
        public Cache(string file) {
            _cacheFile = Path.GetFileName(file);
            if (_cacheFile != file)
                _cacheDir = Path.GetDirectoryName(file);
            Load();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Gets a json string, either from the web or from the local cache. If from the web, it gets cached.
        /// Call with <see cref="MonoBehaviour.StartCoroutine(IEnumerator)"/>.
        /// </summary>
        /// <param name="url">The shortened url, i.e. without the base path.</param>
        /// <returns>Used for coroutine</returns>
        public IEnumerator GetJson(string url) {
            var key = DataFetcher.GetMinimalUrl(url);
            if (!_cache.ContainsKey(key)) {
                yield return _fetcher.GetJson(key);
                _cache[key] = _fetcher.LastJson;
            } else
                yield return null;
            _lastJson = _cache[key];
        }

        /// <summary>
        /// Retrieves the url from the net (or cache) and returns the deserialized object of it.
        /// </summary>
        /// <typeparam name="T">Type to deserialize to</typeparam>
        /// <param name="url">Url to get json from</param>
        /// <returns>The deserialized object</returns>
        public IEnumerator GetObject<T>(string url) {
            yield return GetJson(url);
            yield return JsonUtility.FromJson<T>(_lastJson);
        }

        /// <summary>
        /// Clears the cache. Should only be called when absolutely necessary.
        /// </summary>
        public void Clear() {
            _cache.Clear();
        }

        /// <summary>
        /// Saves the cache to disk in <see cref="CacheDir"/>/ with filename <see cref="CacheFile"/>.
        /// </summary>
        public void Save() {
            var tmp = new UniDictionary<string, string>(_cache);
            var json = JsonUtility.ToJson(tmp, true);
            File.WriteAllText(_cacheFile, json);
        }

        /// <summary>
        /// Loads the cache <see cref="CacheDir"/>/<see cref="CacheFile"/> from disk.
        /// </summary>
        public void Load() {
            if (!File.Exists(_cacheFile))
                return;
            var tmp = JsonUtility.FromJson<UniDictionary<string, string>>(File.ReadAllText(_cacheFile));
            _cache = tmp.ToDictionary();
        }
        #endregion
    }
}
