﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PokeApi.Web {
    //TODO: take ItemSprites.default_ into account
    public class DataFetcher {
        #region Fields

        private const string _baseUrl = "http://pokeapi.co/api/v2/";
        private const char _slash = '/';
        private string _lastJson;
        private Dictionary<string, string> _replacements = new Dictionary<string, string> {
            {"\"default\":", "\"default_\":"},
            { "\\", "" }
        };
        #endregion

        #region Properties
        /// <summary>
        /// The base url of pokeapi
        /// </summary>
        public string BaseUrl {
            get { return _baseUrl; }
        }
        /// <summary>
        /// The character splitting names in the url
        /// </summary>
        public char Slash {
            get { return _slash; }
        }
        /// <summary>
        /// Last retrieved json string
        /// </summary>
        public string LastJson {
            get { return _lastJson; }
            set { _lastJson = value; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Shortens the url as much as possible by removing <see cref="_baseUrl"/> and all leading and trailing slashes if necessary.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetMinimalUrl(string url) {
            var result = url;
            if (result.StartsWith(_baseUrl))
                result = url.Remove(0, _baseUrl.Length);
            result = result.Trim(_slash);
            return result;
        }

        /// <summary>
        /// Gets the Json string from the web. Call with StartCoroutine.
        /// </summary>
        /// <param name="url">Url to get data from</param>
        /// <returns></returns>
        public IEnumerator GetJson(string url) {
            var www = new WWW(_baseUrl + GetMinimalUrl(url));
            yield return www;
            var txt = www.text;
            foreach (var replacement in _replacements) {
                txt = txt.Replace(replacement.Key, replacement.Value);
            }
            _lastJson = txt;
        }

        /// <summary>
        /// Converts the last retreived Json string to the given type
        /// </summary>
        /// <typeparam name="T">Type to convert to</typeparam>
        /// <returns></returns>
        public T ToObject<T>() {
            return JsonUtility.FromJson<T>(_lastJson);
        }

        #endregion
    }
}
