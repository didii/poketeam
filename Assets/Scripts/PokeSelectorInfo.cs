﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PokeSelectorInfo : MonoBehaviour {

    public Image PokeSprite;
    public Image Primary;
    public Image Secondary;
    public Image Offensive1, Offensive2, Offensive3, Offensive4;
    public Image Hp, Attack, Defense, SpAttack, SpDefense, Speed;

}
