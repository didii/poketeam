﻿using System;
using UnityEngine;
using System.Collections;

namespace Utility {
    public class CoroutineData {
        #region Fields

        private object _result;
        protected readonly IEnumerator _target;
        private readonly Coroutine _coroutine;
        protected bool _isDone = false;
        #endregion

        #region Properties
        public Coroutine Coroutine {
            get { return _coroutine; }
        }

        public object Result {
            get { return _result; }
            set { _result = value; }
        }

        public bool IsDone {
            get { return _isDone; }
        }

        #endregion

        #region Constructor
        public CoroutineData(MonoBehaviour owner, IEnumerator target) {
            this._target = target;
            this._coroutine = owner.StartCoroutine(Run());
        }
        #endregion

        #region Methods

        protected virtual void RunStart() {
            _isDone = false;
        }

        protected virtual void RunMiddle() {}

        protected virtual void RunEnd() {
            _result = _target.Current;
            _isDone = true;
        }

        private IEnumerator Run() {
            RunStart();
            while (_target.MoveNext()) {
                RunMiddle();
                yield return _target.Current;
            }
            RunEnd();
        }
        #endregion
    }

    public class CoroutineData<T> : CoroutineData {
        #region Fields

        private T _result;
        #endregion

        #region Properties
        public new T Result {
            get { return _result; }
            set { _result = value; }
        }
        #endregion

        #region Constructor
        public CoroutineData(MonoBehaviour owner, IEnumerator target)
            : base(owner,target) {}
        #endregion

        #region Methods

        protected override void RunEnd() {
            base.RunEnd();
            _result = (T)base.Result;
        }

        #endregion

    }
}