﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility {
    /// <summary>
    /// Holds an object that can have a limited number of types. Mind that this heavily makes use
    /// of reflection, making it an increadibly slow class to use much.
    /// </summary>
    public class LimitedType {
        #region Fields

        private object _obj = null;
        private Type[] _validTypes;
        private bool _allowNull = true;
        private bool _allowBaseTypes = true;
        private bool _allowInheritedTypes = true;
        #endregion

        #region Properties
        /// <summary>
        /// List of all accepted types. If you use unbound generics, set <see cref="AllowUnboundGeneric"/> to false.
        /// </summary>
        public Type[] ValidTypes {
            get { return _validTypes; }
            set { _validTypes = value; }
        }
        /// <summary>
        /// The type of the current object
        /// </summary>
        public Type CurrentType {
            get { return _obj.GetType(); }
        }
        /// <summary>
        /// Is true when unbound generics are used in <see cref="ValidTypes"/>.
        /// </summary>
        public bool AllowUnboundGeneric {
            get { return _validTypes.Any(t => t.IsGenericTypeDefinition); }
        }
        /// <summary>
        /// Whether <see cref="Assign"/>(null) is allowed. When null is assigned, all valid
        /// reference types can be used in <see cref="Get{T}"/>. Is always false if
        /// <see cref="ValidTypes"/> only contains value types.
        /// </summary>
        public bool AllowNull {
            get { return _allowNull && ValidTypes.Any(t => !t.IsValueType); }
            set { _allowNull = value; }
        }
        /// <summary>
        /// Whether or not you can use <see cref="Assign"/> on base classes of any of the
        /// <see cref="ValidTypes"/>.
        /// </summary>
        public bool AllowBaseTypes {
            get { return _allowBaseTypes; }
            set { _allowBaseTypes = value; }
        }
        /// <summary>
        /// Wheter or not you can use <see cref="Assign"/> on inherited types of any of the <see cref="ValidTypes"/>.
        /// </summary>
        public bool AllowInheritedTypes {
            get { return _allowInheritedTypes; }
            set { _allowInheritedTypes = value; }
        }
        /// <summary>
        /// Whether the stored object is null. Always the case if not yet initialized.
        /// </summary>
        public bool IsNull {
            get { return _obj == null; }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// The only constructor.
        /// </summary>
        /// <param name="types">See <see cref="ValidTypes"/></param>
        /// <param name="allowNull">See <see cref="AllowNull"/></param>
        /// <param name="allowUnboundGeneric">See <see cref="AllowUnboundGeneric"/></param>
        public LimitedType(Type[] types) {
            _validTypes = types;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Check if the given type is the current type of the held object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool IsCurrentType<T>() {
            if (AllowNull && _obj == null)
                return IsValid<T>() && !typeof(T).IsValueType;
            return typeof(T).IsAssignableFrom(CurrentType);
        }
        /// <summary>
        /// Checks if the given type is valid according to <see cref="ValidTypes"/>.
        /// </summary>
        /// <typeparam name="T">The type to check</typeparam>
        /// <returns></returns>
        public bool IsValid<T>() {
            Func<Type, Type, bool> baseCond, inheritCond;
            if (AllowBaseTypes)
                baseCond = (inT, validT) => inT.IsAssignableFrom(validT);
            else
                baseCond = (inT, validT) => inT == validT;
            if (AllowInheritedTypes)
                inheritCond = (inT, validT) => validT.IsAssignableFrom(inT);
            else
                inheritCond = (inT, validT) => inT == validT;
            foreach (var type in _validTypes) {
                var inType = typeof(T);
                if (type.IsGenericTypeDefinition && inType.IsGenericType)
                    inType = inType.GetGenericTypeDefinition() ?? inType;
                if (baseCond(inType, type) || inheritCond(inType, type))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Assigns the given object if the type is correct.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <exception cref="InvalidLimitedTypeException"></exception>
        public void Assign<T>(T obj) {
            if (obj == null && !AllowNull)
                throw new ArgumentNullException();
            if (obj != null && !IsValid<T>())
                throw new InvalidLimitedTypeException(this, typeof(T));
            _obj = obj;
        }
        /// <summary>
        /// Alias for <see cref="Assign{T}"/>(null).
        /// </summary>
        /// <param name="obj"></param>
        public void Assign(object obj) {
            Assign<object>(obj);
        }

        /// <summary>
        /// Gets the object back if the type is correct.
        /// </summary>
        /// <typeparam name="T">The type to cast the object to</typeparam>
        /// <returns></returns>
        /// <exception cref="InvalidLimitedTypeException">Thrown if given type is not in <see cref="ValidTypes"/></exception>
        /// <exception cref="InvalidCastException">Thrown if given type is not the same as used in <see cref="Assign{T}"/></exception>
        public T Get<T>() {
            if (!AllowNull && _obj == null)
                throw new NullReferenceException("Cannot be null");
            if (!IsCurrentType<T>())
                throw new InvalidLimitedTypeException(this, typeof(T));
            return (T)_obj;
        }
        #endregion

    }

    /// <summary>
    /// Exception thrown when the casted type does not match
    /// </summary>
    public class InvalidLimitedTypeException : Exception {
        public readonly Type[] ValidTypes;
        public readonly Type GivenType;

        public InvalidLimitedTypeException() : base() { }
        public InvalidLimitedTypeException(string message) : base(message) { }
        public InvalidLimitedTypeException(LimitedType lt, Type givenType)
            : base(
                "Expected one of type (" + (lt.AllowNull ? "null, " : "") +
                lt.ValidTypes.Select(t => t.Name).Aggregate((t1, t2) => t1 + ", " + t2) + ") but got " + givenType.Name +
                " instead") {}
        public InvalidLimitedTypeException(string message, Type[] types, Type givenType) : base(message) {
            ValidTypes = types;
            GivenType = givenType;
        }
        public InvalidLimitedTypeException(string message, Exception innerException) : base(message, innerException) { }
        public InvalidLimitedTypeException(string message, Type[] types, Type givenType, Exception innerException) : base(message, innerException) {
            ValidTypes = types;
            GivenType = givenType;
        }

    }
}
