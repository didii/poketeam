﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UniDictionary<TKey, TValue> {

    #region Fields
    [SerializeField]
    private List<TKey> keys = new List<TKey>();

    [SerializeField]
    private List<TValue> values = new List<TValue>();
    #endregion

    #region Constructor

    public UniDictionary() {}

    public UniDictionary(Dictionary<TKey, TValue> dictionary) {
        foreach (var key in dictionary.Keys)
            Add(key, dictionary[key]);
    }

    #endregion

    #region Properties

    public TValue this[TKey key] {
        get {
            if (!keys.Contains(key))
                throw new KeyNotFoundException();
            return values[keys.IndexOf(key)];
        }
        set {
            Add(key, value);
            values[keys.IndexOf(key)] = value;
        }
    }
    #endregion

    #region Methods
    public void Add(TKey key, TValue value) {
        if (keys.Contains(key))
            return;

        keys.Add(key);
        values.Add(value);
    }

    public void Remove(TKey key) {
        if (!keys.Contains(key))
            return;

        int index = keys.IndexOf(key);

        keys.RemoveAt(index);
        values.RemoveAt(index);
    }

    public bool TryGetValue(TKey key, out TValue value) {
        if (keys.Count != values.Count) {
            keys.Clear();
            values.Clear();
            value = default(TValue);
            return false;
        }

        if (!keys.Contains(key)) {
            value = default(TValue);
            return false;
        }

        int index = keys.IndexOf(key);
        value = values[index];

        return true;
    }

    public Dictionary<TKey, TValue> ToDictionary() {
        var result = new Dictionary<TKey, TValue>();
        for (int i = 0; i < keys.Count; i++)
            result.Add(keys[i], values[i]);
        return result;
    }
    #endregion


}